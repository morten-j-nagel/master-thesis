
\section{Numerical heating and switch modifications}
\label{sec:numer-heat-switch}

Numerical heating is an effect of summed integration errors and can
therefore not be avoided.  In time these errors destroy the
conservation properties.  Frequently, they result in a gain in
temperature, called \textit{numerical heating}.  As the integration
errors should get smaller with a smaller time step they can also be a
limiting factor for the time step.  The evolution of the total system
energy as a function of time was investigated for a surface in the
absence of any barostat or thermostat, which would add or extract
energy from the system.  This is shown in Figure \ref{fig:heating}.
The fit using a linear regression reveals a gain in energy of
\begin{align}
  \label{eq:94}
  \Delta E &\approx \SI{0.14}{\eV\per\pico\second}.
\end{align}

Remarkable is here that the numerical heating is independent of the
time step.  Therefore, it is assumed that this rise is not due to the
normal integration errors but is superposed by an implementation error
or error in the potential.  A possible candidate is the discontinuity
in the second derivative of the switch discussed for binary collisions
in section~\ref{sec:potent-valid}.

\begin{figure}[htb]
  \captionsetup{indent=-2cm}
  \centering \centerline{%
    \begin{subfigure}[cb]{0.55\textwidth}
      \centering
      \caption{heating depending on time step}%
      \label{fig:heat_time}%
      \input{plots/results/nve_heating/nve2}
    \end{subfigure}%
    \begin{subfigure}[cb]{0.55\textwidth}
      \centering
      \caption{heating depending on switch}%
      \label{fig:heat_switch}%
      \input{plots/results/nve_heating/nve3}
    \end{subfigure}%
     }
  \caption{Evolution of the total system energy as a function of time
    for a surface of 2000 particle without any thermostats.  The left
    figure shows the independence of numerical heating from the chosen
    time step.  In the right figure the influence of different switch
    functions is shown. }
  \label{fig:heating}
\end{figure}

Following the analysis of binary collision in the previous section it
was tried to change the switching term from eq.~\eqref{eq:7} to a
function that is also zero in the second derivative (see
Appendix~\ref{sec:deriv-new-switch}).  The system of equations for the
boundary conditions was solved with Splines, Hermite and Chebyshev
polynomials.

The spline interpolation is not continuous in the second derivative
and therefore no improvement was obtained.  The solution of the
Hermite interpolation is satisfying and additional polynomials are
computationally cheaper than cosine-functions.  Finally, the solution
of the Chebyshev interpolation was chosen due to its analytic
similarity to the original switching function.  Without the Heaviside
functions and $t\left(r_{ij}\right) = \frac{r_{ij} - r^{
    \mathtt{min}}_{ij}}{r^{ \mathtt{max}}_{ij} -
  r^{\mathtt{min}}_{ij}}$ follows
\begin{align}
  \label{eq:71}
  \tilde f_c = \frac{1}{4}\Bigg[ 2+ \cos \Big( \pi \cdot
  t\left(r_{ij}\right) \Big) \cdot \Big[ 3 - \cos^2 \Big( \pi \cdot
  t\left(r_{ij}\right) \Big) \Big]\Bigg].
\end{align}

As can be seen in Figure \ref{fig:heat_switch} the new switch
\eqref{eq:71} does not reduce the numerical heating, but in fact
increases it.  The error made with the original switch is a cooling term,
which is quite uncommon.  On the other hand the new switch is
computationally more costly as some more operations have to be made to
calculate it.  While this seems insignificant at first sight, this
function is called up to a million times per time step.  Therefore,
even one additional operation will have a negative impact.

It must be admitted that the source of the heating could not be
identified.  Possible sources of the heating are contribution to
potential not accessible by the binary collisions tests, like local
coordination influence on the bond-order term, or truncation effects
of the Lennard-Jones potential.

For the runs to calculate the sticking coefficients numerical heating
is not a problem as the net gain in energy per bond ($\le
\SI{0.5}{\milli\eV}$) is negligible due to the short simulated times
of \SI{5}{\pico\second}.  In addition, these runs have also active
thermostats.  Further details will be presented in section
\ref{sec:sticking-results}.

For other applications numerical heating is quite limiting as it
prevents reasonable long simulation runs without a thermostat.  Thus,
the process of surface generation is performed with at least some
regions of the simulation box with barostats and/or thermostats as
will be shown in the following section.

%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t
%%% TeX-master: "../main"
%%% End: 
