\section{Validation of the implemented changes}
\label{sec:potent-valid}

For the simulation of the fluorine carbon system no potential was
available in LAMMPS\cite{plimpton1995fast}.  Therefore, additional
routines for an adequate potential had to be implemented into the
source code.  Extending the existing AIREBO-potential of LAMMPS from
two to three species was necessary. The coefficients for
carbon-carbon, carbon-hydrogen and hydrogen-hydrogen interaction were
already existing in LAMMPS.  The ones for carbon-fluorine and
fluorine-fluorine interaction were taken from the open source code of
Jang et~al.\ \cite{jang2004molecular}.  All Lennard-Jones interactions
are also included from there.

After this extension a validation of the implementation of the
potential is needed.  This is not a validation of the potential
itself, because functional forms and parameters were taken as
predefined from literature.  For this test the same atomization
energies of the formed molecules as Jang et~al.\ were used (see
Fig. \ref{fig:atom_small}).  While for hydrocarbons the agreement is
fairly well for fluorocarbons some differences exist.  These
differences were resolved and accepted after direct consultation with
the Sinnott group.

\begin{figure}[htb]
  \centering
  \includegraphics{tikz/appendix/atomization_energies/selected-crop}
  \caption{Differences in atomization energies.  Complete list see
    Figure~\ref{fig:atom_all}.}
  \label{fig:atom_small}
\end{figure}

Tests for bond-lengths and molecular geometries for selected species
showed fairly good agreement with one exception.  \ce{CF2} is linear
when computed with this potential despite it is bend in reality.

An additional test that was performed successfully is a translation
test.  Here, a molecule is moving at a speed of
\SI{100}{\angstrom\per\pico\second} through the simulation box with
periodic boundaries for a long time period.  The test is passed if the
molecule remains stable and only little energy is transferred into the
molecular internal structure.  An extension of this is the addition of
rotational energy.  As molecules like \ce{CF4} are stable with kinetic
and/or rotational energy larger than their atomization energy, this
test succeeded.

Additional testing of the implementation is done by direct probing of
the potential and binary collisions.  Direct probing means, that atoms
were inserted at certain positions and the calculated potential was
compared to the expected one.  The agreement is quite satisfying.


%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t
%%% TeX-master: "../main"
%%% End: 
