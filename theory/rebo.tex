
\section{Reactive Empirical Bond-Order Potential}
\label{sec:react-empir-bond}

Brenner switches from a calculation of the energy in eq.~\eqref{eq:4}
\textit{based on atoms} to a calculation \textit{ based on bonds}
\begin{align}
  \label{eq:10}
  \Phi_i &= \sum_{j \, (> i)} V_{R}(r_{ij}) - b_{ij} V_{A}(r_{ij})
\end{align}
with
\begin{align}
  \label{eq:11}
  b_{ij} &= \frac{\tilde{b}_{ij} + \tilde{b}_{ji}}{2} \\
  &= \frac{b_{ij}^{\sigma - \pi} + b_{ji}^{\sigma - \pi}}{2} \,
  . \nonumber
\end{align}
To achieve that a bond will have the characteristics of a double or
triple bond the term $\pi_{ij}(\ddots)$ is added, if the local
coordination of the bond-forming carbon atoms allows it.  This term
attributes to the radical and conjugate behavior of the system. As the
electrons are completely neglected the effect of conjugation, a
quantum mechanically effect caused by delocalized electrons, is
treated in purely geometrical manner
\begin{align}
  \label{eq:13}
  b_{ij} &= \frac{b_{ij}^{\sigma - \pi} + b_{ji}^{\sigma - \pi}}{2} +
  \pi_{ij}^{rc}.
\end{align}
Likewise, $\pi_{ij}^{rc}$ is determined by a tricubic spline $
F_{ij}\left(N_i^{(t)}, N_j^{(t)}, N_{ij}^{conj} \right)$
\cite{lekien2005tricubic}.  The arguments of this spline are the total
number of neighbors for both bond forming atoms
$N_i^{(t)}$,$N_j^{(t)}$ and the number of carbon neighbor atoms for
both carbon atoms $N_{ij}^{conj}$.  $N_{ij}^{conj}$ attributes to the
conjugated state of the bond (see eq.~\eqref{eq:16}).  The spline is
fitted to known molecular structures and binding energies.

Here, $N_x^t$ represents the total number or neighbors for atom $x$,
which is derived via
\begin{align}
  \label{eq:8}
  N_x^{(t)} &= N_x^{(H)} +  N_x^{(C)}, \\
  \label{eq:14}
  N_x^{(H)} &= \sum_{l}^{hydrogen} f_{xl}(r_{xl}) \\
  \label{eq:15}
  N_x^{(C)} &= \sum_{l}^{carbon} f_{xl}(r_{xl})
\end{align}
where for $f_{xl}(r_{xl})$ the same switching function \eqref{eq:7} is
used.  However, not a single cut-off area is taken into account, but
for each different species combination an individual cut-offlength is
defined.

In the parameter $N_{ij}^{conj}$ for carbon atoms the conjugation
state is specified in the following way
\begin{align}
  \label{eq:16}
  N_{ij}^{conj} &= 1 +\left[ \sum_{k ~\neq i,j}^{carbon}
    f_{ik}(r_{ik}) F(x_{ik}) \right]^\delta + \left[ \sum_{l ~\neq
      i,j}^{carbon} f_{jl}(r_{jl}) F(x_{jl}) \right]^\delta
\end{align}
with
\begin{align}
  \label{eq:17}
  F_{ik} &= \Theta\left(2-x_{ik}\right) +
  \Theta\left(x_{ik}-2\right)\Theta\left(3-x_{ik}\right) \,\mabra{
    \frac{1 + \cos \big(2 \pi (x_{ik} - 2) \big)}{2}}
  % \begin{dcases}
  %   1&, ~ x_{ik} < 2\\
  %   \frac{1 + \cos \big(2 \pi (x_{ik} - 2) \big)}{2} &, ~2 < x_{ik}
  %   < 3 \\
  %   0&, x_{ik} > 3
  % \end{dcases}
\end{align}
and
\begin{align*}
  x_{ik} &= N_k^{(t)} - f_{ik}(r_{ik})
\end{align*}

The bond order term from \eqref{eq:9} is transformed to
\begin{align}
  \label{eq:12}
  b_{ij}^{\sigma - \pi} &= \left[ 1 + \sum_{k (\neq i,j) } f_c(r_{ik})
    g_c(\cos \vartheta_{ijk}) \exp^{\lambda_{ijk} [(r_{ij} -R^e_{ij})
      - (r_{ik} -R^e_{ik})] } + P_{ij} \left(N_i^C, N_i^H\right)
  \right]^{-\delta}
\end{align}

The differences to Eq.~\eqref{eq:9} are:
\begin{itemize}
\item $n=1$
\item the order of $r_{ij} - r_{ik}$ in the exponential function
  $\zeta_{ij}$ is decreased to one
\item a correction term $P_{ij}$ added.
\end{itemize}

$P_{ij}$ describes the difference of the bond energy to the solid
state-case.  This term describes the possibility to form double or
triple bounds in carbon.  For a better description of such bonds in
the fitting procedure additional molecules with double and triple
bonds are included.

In 2001 Brenner et~al.\ published an extended version of this
potential\cite{brenner2002second}.  The main differences to the first
generation potential are discussed in the following.

Most notable is the change of the repulsive and attractive pair-terms
\eqref{eq:5} and \eqref{eq:6} to the form of
\begin{subequations}
  \begin{align}
    \label{eq:18}
    V_{R} (r_{ij}) & = f_{ij}(r_{ij})\, \left( 1 +
      \frac{Q_{ij}}{r_{ij}} \right) \, A_{ij}
    \exp \left( - \alpha_{ij} r_{ij} \right), \\
    \label{eq:19}
    V_{A} (r_{ij}) & = - f_{ij}(r_{ij})\, \sum_{n=1}^3 \, B_{ij}^{(n)}
    \, \exp \left( - \beta_{ij}^{(n)} r_{ij} \right),
  \end{align}
\end{subequations}
which were chosen by \textit{``trial and error''}\cite{brenner2005} to
produce a more accurate potential. More fitting parameters allow to
consider more bond lengths, energies and force constants.  The
resulting potential together with the extension for fluorine by
Jang\cite{jang2004molecular} is shown in Fig. \ref{fig:potential}.
Jang's coefficients are taken as they are also include hydrogen, not
like the one from Tanaka\cite{tanaka:938}, which only includes carbon
and fluorine.  These potential curves match the exact curves in
reality but are more an approximation\cite{doi:10.1021jp004368u}.

\begin{figure}
  \captionsetup{indent=-2cm}
  \centering
  \centerline{\input{plots/results/time_step_est/basic_interaction}}
  \caption{The intramolecular part of the used potential
    (\eqref{eq:10} with \eqref{eq:18} and \eqref{eq:19}).  Here the
    carbon-carbon, fluorine-fluorine and carbon-fluorine interactions
    are plotted on a per-bond basis. As the bond order term is shown
    with $b_{ij} = 1$ the form of the potential will vary exceedingly
    depending on the researched structure. Also one can see the strong
    influence of the switching function. }
  \label{fig:potential}
\end{figure}

Further changes are in the term $\pi_{ij}^{rc}$ in eq~\eqref{eq:13}
representing the radical character of a bond to which a further term
$\pi_{ij}^{dh}$ for calculating the influence of the dihedral angle in
a carbon-carbon double bond is added.  This results in the new
bond-order term
\begin{align}
  \label{eq:20}
  b_{ij} &= \frac{b_{ij}^{\sigma - \pi} + b_{ji}^{\sigma - \pi}}{2} +
  \pi_{ij}^{rc} + \pi_{ij}^{dh}.
\end{align}

While in principle the terms describing the $\sigma$ and $\pi$ bonds
$b_{ij}^{\sigma - \pi}$ from Eq.~\eqref{eq:12} remains the same, in
the exponential term the distance dependence is included in the
variable and $\delta$ is chosen as $1/2$ leading to
\begin{align}
  \label{eq:22}
  b_{ij}^{\sigma - \pi} &= \left[ 1 + \sum_{k \,(\neq i,j) }
    f_c\left(r_{ik}\right) \,g_c\left(\cos \vartheta_{ijk}\right) \,
    \exp^{\lambda_{ijk}} \, + \, P_{ij} \left(N_i^C, N_i^H\right)
  \right]^{-\frac{1}{2}}
\end{align}
with
\begin{align}
  \label{eq:29}
  \lambda_{jik} &= \sum_{x = H,\dots} \, 4 \delta_{ix} \left[ r_{ij} -
    r_{ik} + \sum_{y = C,H, \dots} \rho_{yx} \left(\delta_{ky} -
      \delta_{jy} \right) \right] \, .
\end{align}

The correction term $ P_{ij} \left(N_i^C, N_i^H\right)$ is necessary
to take into account different chemistry of bonds depending on the
density of specific species.  $P_{ij}$ is a cubic spline through
provided data points.  While in the original
papers\cite{brenner2002second} only the interaction of two species,
carbon and hydrogen atoms, is considered, increasing the
dimensionality of this cubic spline allows to add more species like
fluorine \cite{jang2004molecular}.  All other terms depend only on the
species of two atoms or the total density of atoms in the direct
neighborhood.  Therefore, $ P_{ij} \left(N_i^C, N_i^H\right)$ has to
include the number of Fluorine atoms to $ P_{ij} \left(N_i^C, N_i^H,
  N_i^F\right)$.
% \todo{Satzende fehlt}

In this expression the angular dependence is changed from
eq. \eqref{eq:9} to
\begin{align}
  \label{eq:21}
  g_c ( \cos \vartheta )&= G_C( \cos \vartheta )+ Q \left( N_i^{(t)}
  \right) \left[ \gamma_C( \cos \vartheta ) - G_C( \cos \vartheta )
  \right]
\end{align}
in which $G_C$ is a sixth-order polynomial spline.
% \todo{Satz klingt merkwürdig: "To regarded..."}
The low-coordination structure $\gamma_C$, which is obtained through
fitting of small angle configurations, is taken into account by the
local coordination for \textit{atom i}
\begin{align}
  \label{eq:24}
  N_{i}^{(t)} &= N_{i}^{C} + N_{i}^{H} + \dots
\end{align}
and the switching function
\begin{align}
  \label{eq:23}
  Q_i\left(N_{i}^{(t)}\right) &= \Theta\left(3.2 - N_{i}^{(t)}\right)
  + \Theta\left(N_{i}^{(t)} - 3.2\right) \Theta\left(3.7 -
    N_{i}^{(t)}\right)\frac{1 + \cos{\Big[ \, 2 \pi \big( N_{i}^{(t)}
      - 3.2 \big) \Big]}}{2}
  % \\
  % &=\begin{dcases}
  %   1&, \qquad N_{i}^{(t)} ~<~ 3.2 \\
  %   \frac{1 + \cos{\left[2 \pi \left( N_{i}^{(t)} - 3.2 \right)
  %   \right]}}{2}&,\qquad 3.2 ~<~ N_{i}^{(t)} ~<~ 3.7\\
  %   0&, \qquad N_{i}^{(t)} ~>~ 3.7.
  % \end{dcases}
\end{align}

The third term in eq. \eqref{eq:13} describes the radical and
conjugated character of the bond and is given via a tricubic spline
fit through the number of hydrogen and carbon neighbors and
eq. \eqref{eq:16}, in which $\delta$ is changed to $\delta = 2$.  Also
the fitting species are changed so different data points are included.

A complete new addition is the fourth term $\pi_{ij}^{dh}$ specifying
the dihedral limitation of a carbon-carbon double bond.

\begin{align}
  \label{eq:25}
  \pi_{ij}^{dh} &= T_{ij}\left(N_i^{(t)}, N_j^{(t)}, N_{ij}^{conj}
  \right) \left[ \sum_{k \, ( \neq i,j) } \: \sum_{l \, ( \neq i,j) }
    \left( 1 - \cos^2{\vartheta_{ijkl}} \right) \, f^C_{ik}\left(
      r_{ik}\right)f^C_{jl}\left( r_{jl}\right)\right]
\end{align}
with
\begin{align}
  \label{eq:26}
  \vartheta_{ijkl} &= \frac{\vec{r_{ji}} \times \vec{r_{ik}} }
  {\left\| \vec{r_{ji}} \times \vec{r_{ik}} \right\|_2 } \, \cdot \,
  \frac{\vec{r_{ij}} \times \vec{r_{jl}} } {\left\| \vec{r_{ij}}
      \times \vec{r_{jl}} \right\|_2 }
\end{align}

The parameters are fitted to various species, structures and
reactions, which are often obtained via first principle calculations
or measurements, to get a potential as accurate as possible.  Up to
now only \textit{inter-molecular} effects are included, but no
\textit{intra-molecular} effects.  Therefore, a pure reactive bond
order potential is most suitable for cases, where solid state or high
connected phases are described.  For weakly bond molecules like films
or the interaction between layers in graphite this limitation may lead
to unphysical results.  Electronic force contribution due to charge
distribution effects need to be included.

A computationally cheap and also established method are the so called
\textit{12-6 Lennard-Jones potentials} like introduced in section
\ref{sec:inter-potent}.  Here, the attractive term of a dipole scaling
with $r^{-6}$ acts against a repulsive term scaling with $r^{-12}$,
which represents the repulsive forces of overlapping orbitals.  As
this potential alone would prevent a transition from the large
distance of an \textit{intermolecular} interaction to the short
distance of an \textit{intramolecular} interaction, it is necessary to
handle switching of the Lennard-Jones-Potential.  This is determined
by the current bonding state specified by the bond-order-term and the
position relative to each other in a molecule for the two interacting
atoms.  In the following section the inclusion of this \textit{12-6
  Lennard-Jones potentials} into the reactive bond order will be
presented leading to the \textit{adaptive intermolecular bond order
  potential}.

%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t
%%% TeX-master: "../main"
%%% End: 
