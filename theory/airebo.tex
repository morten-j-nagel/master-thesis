
\section[AIREBO-Potential]{Adaptive Intermolecular Reactive Empirical
  Bond Order Potential}
\label{sec:adapt-interm-react}
% \sectionmark{AIREBO-Potential}

To include the intermolecular interactions in the reactive empirical
bond-order (REBO) potential Stuart, Tutein and
Harrison\cite{stuart2000reactive} proposed the inclusion of a
potential based on a $6-12$ Lennard-Jones potential. The resulting
potential is called \textit{adaptive intermolecular reactive bond
  order}-potential (AIREBO) and will be presented in the following.  In
the original AIREBO-potential an additional term is added to
contribute to the torsional limitation of specific carbon-carbon
bonds. 

The used $6-12$ Lennard-Jones potential is
\begin{align}
  \label{eq:27}
  \Phi_{ij}^{LJ}\left(r_{ij}\right) &= 4 \, \epsilon_{ij} \left[
    \left( \frac{\sigma_{ij}}{r_{ij}}\right)^{12} - \left(
      \frac{\sigma_{ij}}{r_{ij}}\right)^{6} \right]
\end{align}
where the values are given in Table~\ref{tab:lj1} and are chosen from 
\cite{stuart2000reactive} for the $C-H $-interaction and from
\cite{jang2004molecular} for the C-F and H-F interaction.  This is the
same potential but with different coefficients like the one for water
in Fig. \ref{fig:LJ_water}.  While the form remains identical the
other coefficients shift the equilibrium distance and scale the
potential depth.  It should be noted that the binding energy or
more precisely the potential minimum for the interaction of only two
atoms is only a fraction of the binding energy of an intermolecular bond
between the two (like a factor $10^{-3}$).  But due to the minimum
located at greater distance, each atom  can have more
intermolecular interacting neighbors leading to effective binding
energies well above the kinetic energy of a molecule.

The form of eq. \eqref{eq:27} tends to
infinity for $r \rightarrow 0$.  Because $\sigma_{ij}$ is
normally larger than the normal bond-length without further
terms one would get a too high barrier preventing the
formation and breaking of bonds.  Due to the reactive
behavior of the system there exist cases, where
the Lennard-Jones potential has to be reduced.  Such cases with small
Lennard-Jones interactions are:

\begin{enumerate}
\item distance so small making the formation of a bond possible,
\item low coordination number with the possibility of forming a
  bond,
\item close atoms in the same molecule.
\end{enumerate}

These requirements are fulfilled by the following term
\begin{align}
  \label{eq:31}
  \mathcal{E}_{ij}^{\mathtt{LJ}} &= C_{ij} \,
  \Phi_{ij}^{LJ}\left(r_{ij}\right) \Bigg[ 1 ~ - ~ f_{LJ}\left(t_r
    \left( r_{ij} \right) \right) \cdot \left[ 1 - f_{LJ}\big( t_b
    \big( b_{ij}^\ast \big) \big) \right] \Bigg]
\end{align}

\begin{figure}[bht]
  \centering
  \input{plots/theory/bij_influence_LJ/lennard-jones-switch}
  \caption{Influence of the bond-order term on the Lennard-Jones part
    of the potential}
  \label{fig:switch_bij_LJ}
\end{figure}

in which the first request is implemented by the switching function
depending on the distance $f_{LJ}(r_{ij})$, the second request by the
switching function depending on a hypothetical $f_{LJ}(b^*_{ij})$ and
the third request by the connectivity switch $C_{ij}$.  The derivation
of these terms will be explained in the following (for visualization
see fig.~\ref{fig:switch_bij_LJ}).

The first case is attributed to a distance-depending switch,
which slightly differs from eq.~\eqref{eq:7} in the original
REBO-potential (see also Sec.~\ref{sec:numer-heat-switch})
\begin{align}
  \label{eq:28}
  f_{LJ}(t) &= \Theta(-t) +
  \Theta(t)\Theta(1-t)\left[1-t^2(3-2t)\right]
\end{align}
with transition region $r^{\mathtt{max}}_{ij}$ and
$r^{\mathtt{min}}_{ij}$ normalized to the interval $[0,1]$
\begin{align}
  \label{eq:30}
  t_r \left( r_{ij} \right) &= \frac{r_{ij} - r^{\mathtt{LJ} ~
      \mathtt{min}}_{ij}}{r^{\mathtt{LJ} ~ \mathtt{max}}_{ij} -
    r^{\mathtt{LJ} ~ \mathtt{min}}_{ij}}.
\end{align}
Here, \eqref{eq:30} inserted in the switching function \eqref{eq:28}
leads to an unperturbed Lennard-Jones potential for distances greater
than $r^{\mathtt{max}}_{ij}$. In the transition region the
Lennard-Jones potential goes to zero close to
$r^{\mathtt{min}}_{ij}$.  The two other request 2) and 3) can modify
this.

The borders of the transition area are defined as
\begin{align}
  \label{eq:33}
  r^{\mathtt{LJ} ~\mathtt{min}}_{ij} = \sigma_{ij} \qquad \mathtt{and}
  \qquad r^{\mathtt{LJ} ~\mathtt{max}}_{ij} = 2^\frac{1}{6}
  \sigma_{ij}
\end{align}
from the potential minimum.  This leads to an unperturbed potential
minimum and also a continuous second derivative at $r^{\mathtt{min}}$.

As already pointed out in Sections \ref{sec:react-empir-bond} and
\ref{sec:tersoff-potential} the number of bonds is limited in
Tersoff-like potentials like the AIREBO-potential by the bond-order
term $b_{ij}$ from eq.~\eqref{eq:13}.  This term is multiplied with
the attractive component of the potential and includes the local
coordination number of two interacting atoms. It is high, if both
atoms have the possibility to form further bonds, causing this way a
strong attraction between the two. It is low, if a bond formation is
unlikely.
\begin{SCfigure}%[hbt]
  \captionsetup{indent=0.5cm}
  \centering\hskip .75cm \input{plots/theory/est_bij/bij_est}
  \caption{Estimation of the characteristic bond-order term for
    reactive (\ce{CF} and \ce{CF3}) and non-reactive species (\ce{CF4}
    as shown in the right).  The red dots are random position at which
    the hypothetical bond-order term \eqref{eq:35} is evaluated.}
  \label{fig:est_bij}
\end{SCfigure}
Since the second request is the switching-off of the repulsive barrier for
cases with low local coordination number, as the formation of a bond
between two radicals is likely, another switching function
$f_{LJ}\big( t_b \big( b_{ij}^\ast \big)$ is added to the
Lennard-Jones potential.  Here, for the hypothetical term
\begin{align}
  \label{eq:35}
  b_{ij}^\ast &= b_{ij} \Big|_{\,r_{ij}~ = ~r_{ij}^{\mathtt{min}}}
\end{align}
the local coordination is calculated.
% \todo{der Satz klingt komisch: like they ... by ... the would}
It is assumed that both radicals are separated by
$r_{ij}^{\mathtt{min}}$.  With
\begin{align}
  \label{eq:32}
  t_b\left( b^\ast_{ij} \right) &= \frac{b^\ast_{ij} -
    b^{\mathtt{min}}_{ij}}{b^{\mathtt{max}}_{ij} -
    b^{\mathtt{min}}_{ij}}
\end{align}
the barrier is absent for bond-order terms greater than
$b^{\mathtt{max}}_{ij}$ and full active for bond-order terms smaller
than $b^{\mathtt{min}}_{ij}$.  In between a smooth transition is
achieved via the switch-off function \eqref{eq:28}.  As the
bond-length and bonding characteristics are type-dependent for every
combination the maximum and minimum have to be determined like it was
done by Stuart et~al.\ \cite{stuart2000reactive}.  They fitted the C-H
interaction  by calculating the bond-order term for a hydrogen
atom approaching a methane and a methyl radical.  From the first case,
where a reaction is very improbable, the lower limit of
$b^{\mathtt{min}}_{ij} = 0.75$ is derived, while the highly
reactive second case leads to the upper limit of
$b^{\mathtt{max}}_{ij} = 0.90$.  A complete list of all used
parameters is given in table~\ref{tab:lj1}.

At last to fulfill the third request of no \textit{intermolecular}
interaction for atoms located ``near'' to each other in the same
molecule, the term
\begin{align}
  \label{eq:34}
  C_{ij} &= 1 - \max\left\{w_{ij}(r_{ij}),~
    w_{ik}(r_{ik})w_{jk}(r_{jk}) ~\forall k,~
    w_{ik}(r_{ik})w_{kl}(r_{kl})w_{lj}(r_{lj}) ~\forall k\right\}
\end{align}
is included in which $w_{ij}(r_{ij}) = f_{C} (r_{ij})$ is defined by
eq. \eqref{eq:7}.  By this, the switching function \eqref{eq:28} from
the Lennard-Jones potential is no longer used but the switching
function of the REBO-potential.  With \eqref{eq:34} atoms with two,
one or no intermediate neighbors do not longer interact.  In these
cases $C_{ij}$ becomes zero and with it the whole
\textit{intermolecular} potential. 
% \todo{Im letzten Satz fehlen Worte}

This completes the description of the interaction potential and the
simulation technique used in this work.  The next chapter will present
further details about the numerical implementation.

% \todoin{missing: recap of the first chapter and translation to
%   numerical implementation
%   \begin{itemize}
%   \item reactive potential
%   \item intermolecular forces
%   \item plasma properties
%   \item ?
%   \end{itemize}
% }

%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t
%%% TeX-master: "../main"
%%% End: 
