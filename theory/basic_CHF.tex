In the following the basic properties of fluorine carbon plasmas and
their films will be briefly presented based on
Gabriel\cite{gabrielanalyse} and Stepanov\cite{stepanov2010absolute},
where further details can be found.


\section{Carbon Fluorine Plasmas}
\label{sec:carb-fluor-plasm}

\textit{Plasma} is here referred to as a gas in which at least a part
of ingredients is ionized. This is commonly achieved via an energy
input.  A standard experiment for studies of carbon fluorine plasmas
and films consists of a low-pressure reactor ($1-100 ~\text{Pa}$)
filled with stable carbon-fluorine molecules like \cee{CF4},
\cee{C2F6}, \cee{C3F8} or \cee{C4F8}. In case of \cee{CF4} a global
model already exists as discussed already in \ref{sec:Motivation}.
One of the two parallel electrodes is powered with
\SIrange{10}{200}{\watt} at a radio frequency of $13.56 ~\text{MHz}$
and the other one is grounded like the whole reactor wall (see
Figure~\ref{fig:exp_reactor} and \cite{0022-3727-40-23-020}).  In this
frequency range electrons experience the modulated field and respond
following its time dependence due their small mass. They gain energy
and atoms are ionized.  This leads to the formation of the
\textit{plasma discharge}. Ions are much heavier and experience only
the phase averaged electric field moving much slower than electrons.

Only a relatively small number of atoms are ionized, typically below
$1\%$.  Reference parameters of such discharges are a number density
of ions of \SI{e12}{\per\cubic\centi\metre}, a neutral gas density of
\SI{e16}{\per\cubic\centi\metre} at a pressure of \SI{50}{\pascal}.
Plasmas containing fluorine are electronegative discharges due to the
existence of negative ions \cee{F-} and \ce{CF3-}. These negative ions
can even dominate with up to ten negative ions per free electron.
Their formation is happening mainly through dissociative attachment of
electrons on neutral molecules (see reaction \eqref{eq:46} and
\eqref{eq:103}).

A \textit{non-thermal} low-temperature plasma is produced. The ions
and neutrals have a temperature in the order of the room temperature
and the electron temperature is a few \si{\eV} or of the order
\SI{e5}{\kelvin}.  One electronvolt is the amount of energy gained by
an electron passing through a potential drop of \SI{1}{\volt}.  Used
as temperature unit \SI{1}{\eV} is equivalent to
\SI{11604.5}{\kelvin}.  However, as the energy distribution is not
always a Maxwell-Boltzmann distribution the definition of temperature
may not be applied strictly.

Further, in experiments the power is switched off for some time to
modify the surface film properties.  Different switch-off times change
the ratio of energetic and low-energy ions impinging on the surface
and determining by this its properties. The \textit{duty-cycle}
defines the ratio between on and off phases.  

\begin{SCfigure}%[0.5\textwidth][]
  \centering \input{tikz/theory/reactor/reactor}
  \caption{Schematic plot of the reference experiment made by Gunnar
    Bandelow: a reactor with two capacitive coupled radio frequency
    electrodes.  The samples are at example positions with different
    fluxes of species to the surface: a high flux of high energetic
    particles at the electrodes and the plasma and a low flux of
    slow particles to the reactor wall.}
  \label{fig:exp_reactor}
\end{SCfigure}

Ionization or dissociation reactions of fast moving electrons with
ions, molecules or atoms determine the plasma.  Fragments create by
such collisions can be highly reactive leading to the possibility of
further reactions.  A list of reactions is given in the appendix
\ref{sec:plasma-reactions-1}.  Important for this work are the
radicals and ions \cee{F*}, \cee{CF*}, \cee{CF2*}, \cee{CF3*},
\cee{F+}, \cee{CF+}, \cee{CF2+}, \cee{CF3+} and \cee{F-}, \cee{CF3-}.
Mixing of two feed-gases like \cee{CF4} and \cee{H2} can also form a
new species like \ce{HF}.  Therefore, a plasma consists \textit{not
  only of feed-gas molecules} but also of \textit{many other species}.

Even species with very small concentrations can have a significant
influence. If their sources and sinks are not at the same location,
they drive considerable amount of transport in the plasma.  A possible
example is the generation of \cee{CF2} from \cee{CF} at the surface
and the destruction of this \cee{CF2} molecule into \cee{CF} and
\cee{F} in the plasma volume. This results in a net transport of
fluorine from the wall into the plasma.

The development of a zero dimensional model of such plasmas is done by
Bandelow.  In this model for various species reaction coefficients are
included to compute the density evolution of each species as a
function of time.  The model can reproduce the strong production of
\ce{HF} when \ce{H2} is added. However, a quantitative description of
processes like the generation of \ce{C2F4} in the plasma-off phase
could not be achieved yet.  One possible explanation is missing
surface reactions within the model\cite{0022-3727-40-23-020}.

Caused by the high electron mobility due to their small mass these
plasma facing surfaces are charged negatively. In the plasma close to
the surface a positive space charge is formed due to the strongly
reduced mobility of the heavy positive ions compared to the electrons.
This space charge produces a potential drop, the so-called
\textit{plasma sheath}, in which the positive ions will be
accelerated.  They will hit the surfaces with much higher velocity as
their thermal velocity.  In contrast to that, the majority of negative
ions are reflected from the potential wall like the electrons and will
not reach the surface.  Thus, it is not necessary to investigate
surface reactions of negative ions.

Plasma confinement is limited to the region between the electrodes. At
the electrodes high fluxes of positive ions with high impact energies
will interact with the surfaces.  Outside this discharge region
neutral gas without plasma is dominating.  At the reactor wall much
lower fluxes of neutrals with less impact energy compared to the
electrodes will determine the surface reactions.

Depending on their kinetic energy at the wall incoming ions can
sputter surface atoms, can stuck to the surface, can reflect from the
surface or experience surface reactions.  The first case is used for
\textit{plasma etching} to custom-tailor a desired structure like a
deep well on a substrate.  The second case is used for \textit{plasma
  polymerization} where a surface is produced with polymers formed
from species contained in the plasma.  Even if one process dominates
the others are usually still present.  Also the conditions will vary
in an experimental reactor.  Etching may appear on the electrodes
while polymers are formed at the reactor walls.

In the following the surfaces in such discharges will be
characterized.


\section{Properties of amorphous fluorine carbon films}
\label{sec:prop-amorph-fluor}

Surfaces produced in fluorine carbon plasma are generally amorphous
films with different properties, which will be discussed in this
section.  The key parameter for their properties is the fraction of
fluorine to carbon $\kappa = \frac{N_F}{N_C}$ with $N$ being the
number density.  The two extreme cases at which stable surfaces still
exists are pure amorphous carbon ($\kappa = 0$) and
Polytetrafluoroethylene/PTFE ($\kappa = 2$).  For larger $\kappa$ a
stronger network of carbon atoms is built by increased cross linking
with formations of carbon double bonds.  This results in a harder
surface.  Softer fluorine carbon polymers have a higher fluorine
content and consist of polymer fibers twisted into each other.  The
fibers stick together by \textit{intermolecular forces} discussed in
section \ref{sec:van-der-waals}.

The reactivity of a surface is determined by the number of dangling
bonds and the amount of cross-linking.  Dangling bonds are surface
atoms with unsaturated open bonds. A surface with a large amount of
cross-linking will be less flexible and will therefore absorb a
smaller amount of energy per incoming particle compared with a surface
of less cross-linking.  This internal structure will be affected
strongly by the production process of the film.  In case of sputtering
the shrinking film will contain remnants of the structures from the
original material like graphite.  Films grown by deposition will show
less of such remnants.  Surface roughness will influence the surface
properties as well (see \ref{sec:disc-param}).

Experimental results show that $\kappa$ of the surface is mainly
determined by the mean energy of the impinging ions and the fraction
$\kappa$ in the plasma \cite{Liu2010253} or hot wire
reactor\cite{Lau2000119}.  A larger impact energy produces a surface
with less fluorine content.  Other parameters like the flux of ions
and neutrals are likely to have an impact on the characteristics of
the films.  A large enough particle flux density prevents a complete
equilibration of the surface between two particle impacts. The
non-equilibrium situation provides additional energy, e.g. to break
bonds.  This condition is fulfilled if the time difference between
subsequent particles is smaller than the equilibration-time.  The
equilibration is determined by the coupling to phonons resulting in
$\SI{1}{\pico\second}$\cite{lee:813}.  This requires fluxes larger
than \SI{1e24}{\per \square \cm \per \second}.  Compared with the
experimental fluxes\cite{booth:3097} $\approx \SI{1e15}{\per \square
  \cm \per \second}$ the system is far off from these conditions.
Therefore, subsequent impacts can be treated independently from each
other.

Experimental results for films can be summarized as follows.  The
cross-linking is higher with increased amount of $sp^2$ carbon in the
surface\cite{Jung2002248,ma:3353,Ahn20082019}.  The cross-linking gets
larger with higher thermal stability\cite{wang:621}.  The surface
hardness decreases with growing $\kappa$\cite{Chen01012004}.  The surface
temperature influences the carbon hybridization as a higher
temperature results in a higher $sp^2$ fraction\cite{Lu20081871}.
Surface roughness measured in an area of \SI{4}{\square \micro \metre}
produces height variations of \SI{20}{\nano\metre}
\cite{Liu2010253,doi:10.1021/jp067521e}.  Therefore, on microscopic
scale these effects can be neglected.  Deposited films with $\kappa =
0.5$ are obtained at locations where energetic molecules hit the
surface and films with $\kappa \lesssim 2$ at locations with reduced
impact energy \cite{Liu2010253, doi:10.1021/cm0002416,
  APP:APP070271029, CVDE:CVDE200806755}.  One simple way to lower the
impact energy is to reduce the duty cycle.  Standard growth rates are
between \si{1} and \SI{500}{\angstrom \per
  \minute}\cite{doi:10.1021/cm0002416,Liu2010253}.  The densities for
fluorinated alkanes is reported with \SI{1.6}{} to
\SI{2.2}{\gram\per\cubic\cm}\cite{doi:10.1021/ma025645t,yang:1514}.
Surfaces with a $\kappa = 0.5$ are reported in \cite{Liu2010253} and
\cite{yokomichi:2468}.  Practical no chains ends through \ce{-CF3} are
reported for these surfaces and no degeneration effects happen for
annealing temperature of up to \SI{750}{\kelvin}\cite{Ariel2001383}.

\section{Surface reactions}
\label{sec:expect-surf-react}

A surface reactions is characterized by at least one reactant absorbed
at the surface.  There are two possibilities for a surface reaction of
two molecules:

\begin{itemize}
\item Eley-Rideal mechanism
\item Langmuir-Hinshelwood mechanism.
\end{itemize}

In the Eley-Rideal mechanism the second molecule reacts directly from
the gas phase with an absorbed surface atom.  This is most likely a
direct hit by the incoming particle. Due to the need for sufficient
energy for this process this reaction channel is also called
\textit{hot atom mechanism}.  For Langmuir-Hinshelwood both molecules
are absorbed at the surface and get close to each other due to
diffusion.  This process is a thermal random with velocities much
smaller than for Eley-Rideal.  Therefore, this process has Eigentimes
of the diffusion time-scale, which means millisecond.

One interesting feature of surface reactions is their reduced
dimensionality.  While in the plasma three-body-reactions like
\eqref{eq:47} to \eqref{eq:49} are very unlikely, because a third body
is needed to fulfill all conservation equations. The surface can act
as the third reactant which is always present allowing much more
efficient three-body-reactions.

The experimental observation of reduced mean energy of the incoming
particle and increased fluorine concentration of the
surface\cite{Liu2010253} can be understood as a
consequence of changed fluorine atom losses at the surface.  At low
impact energy fluorine-rich surfaces are produced, because possibly
less fluorine is ejected kinematically and more reactions are taking
place.  The \ce{CF2*} radical can establish a bond with the surface at
these low energies and resulting in an fluorine enrichment of the
surface.  In contrast, at large impact energies fluorine is sputtered
and \ce{CF2*} molecules are formed leaving the surface.  Therefore,
the surface can act as a radical sink or source for the
plasma. Interestingly there is experimental evidence for both
cases\cite{sasaki:5585,0963-0252-14-1-001}.
% \todo{Quelle}

For pure PTFE the separate chains are mostly not connected through
covalent bonds.  Major parts of the surface stability are therefore
caused by other \textit{intermolecular forces}.  In the following
section a brief description will be given.

%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t
%%% TeX-master: "../main"
%%% End: 
