
\section{Interaction potentials}
\label{sec:inter-potent}

\textit{Interaction potentials} determine the complete dynamics of the
system.  From these potentials $\Phi_i$ the forces acting on particles
$i$ are derived, e.g.  the Coulomb force or Van-der-Waals-force caused
by the electronic charge of the particles or their polarizability.
From all potentials the total potential energy of the system can
be calculated as the sum over the potential energies of all particles
\begin{align}
  \label{eq:3}
  E_{pot} &= \sum_{i} \Phi_i \,.
\end{align}

To get the force acting on particle $i$ in order to calculate its
movement, the gradient of the potential is calculated
\begin{subequations}
  \begin{align}
    \label{eq:2}
    \vec F_i &= - \vec \nabla_{\vec{r_i}} \Phi_i \\
    \label{eq:75}
    &= m \cdot \vec a_i \,.
  \end{align}
\end{subequations}
This is correct for systems like the one studied in this work where
potentials are only spatially depending, excluding e.g. velocity
dependencies.  Using the forces in the equation of motion numerical
integration delivers the full dynamics of the system.  

A central numeric task is the calculation of distances between
particles.  This determines the force and dynamics.  Usually, a Taylor
expansion of the interaction potential depending on the number of
neighbors is used, the so-called \textit{"m-body potential"}
\begin{align}
  \label{eq:1}
  \Phi_i &= \Phi_i (\vec{r_i}) ~+ \sum_{i \neq j} \Phi_{ij}
  (\vec{r_i}, \vec{r_j}) ~+ \sum_{i \neq j, k} \Phi_{ijk} (\vec{r_i},
  \vec{r_j}, \vec{r_k}) ~+ \dots .
\end{align}

The first expansion term includes contribution from external fields.
A potential consisting only of the first two terms is termed as
\textit{``pair-potential''}.  The pair potentials only depend on the
distance between particles (further noted with \(\vec{r_{ij}} =
\vec{r_i} - \vec{r_j}\) and \( r_{ij} = | r_i - r_j|\)), while more
advanced potentials include properties like the particle density or
the angle between three particles.  The main problem of higher order
potentials are their increasing computational costs, which grows with
expansion order.  For a 3-body potential it scales with $N^3$, where
$N$ is the number of particles in the simulation.

\begin{figure}[bht]
  % \captionsetup{indention=-0.6cm}
  \centering \hskip 2cm \input{plots/h20_lj/h20_lj}
  \caption{Pair potential to describe the intermolecular interactions
    between water molecules from\cite{Barker1969144}.  Due to the
    strong hydrogen bonds in water for an intermolecular potential the
    minimum is quite deep.}
  \label{fig:LJ_water}
\end{figure}

These potentials are capable to describe at least some aspects of
relatively small molecules like water with a Lennard-Jones potential
(see Fig.~\ref{fig:LJ_water}).  The potential is only depending on
\textit{distance}.  \textit{Angular} effects like in
Fig.~\ref{fig:vdwHF} can not be reproduced.  The simulated structure
will therefore be different from the structure of real water.  For
more complex cases like the bonding in carbon molecules, it is
necessary to include higher body-terms.  

There is a huge number of interaction potentials existing,
specifically so-called \textit{force-fields}.  These are empirically
fitted potentials, usually using only harmonic or semi-harmonic
approximation.  They are very popular for drug design and biological
systems due to their simplicity and small computational costs.  One
major short-coming in using such force-fields is their incorrect
description of reaction dynamics due to their simplified form.

In the following section a more complex potential description
overcoming these shortcomings is presented.  The
\textit{AIREBO-potential} will be derived from its ancestor, the
\textit{Tersoff-potential}.

%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t
%%% TeX-master: "../main"
%%% End: 
