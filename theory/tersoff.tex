
\section{Tersoff Potential}
\label{sec:tersoff-potential}

In this section the so-called \textit{``Tersoff-Potential''} will be
introduced. The aim of this work is to understand the reaction of
molecule consisting of fluorine and carbon with surfaces also
consisting of these constituents.  To describe such systems a
potential formulation originally developed by J. Tersoff for silicon
is adapted.

Abell\cite{abell1985empirical} derived a general expression for the
binding energy, which Tersoff\cite{tersoff1986new} used as pair
potential
\begin{align}
  \label{eq:4}
  \Phi_{ij} & = \Bigg[ a_{ij} \cdot V_R(r_{ij}) ~+~\tilde{b}_{ij} \cdot
  V_A(r_{ij})\Bigg ] \,.
\end{align}

The key feature of this potential ~\eqref{eq:4} is the invention of
the so-called \textit{bond-order term} $\tilde{b}_{ij}$.  Its
influence is illustrated in figure~\ref{fig:bond-order-tersoff}.

\begin{figure}[ht]
  \captionsetup{indention=-0.8cm}
  \centering \input{plots/tersoff_bij/tersoff-bij}
  \caption{Influence of the bond-order term $\tilde{b}_{ij}$ introduced
    in eq.~\eqref{eq:4}.  Depending on $\tilde{b}_{ij}$ the potential
    minimum or bond energy as well as the equilibrium distance is
    varied.  Due to $\tilde{b}_{ij}$ depending on various parameters
    (see eq.~\eqref{eq:9}) certain configurations, like a diamond-like
    structure, can be achieved. }
  \label{fig:bond-order-tersoff}
\end{figure}


The bond-order term is determined by the local environment, which
includes implicitly three or higher-body interactions.  This makes a
computational cheap pair-potential sufficient for the description of a
complex problem.  Therefore, $\tilde{b}_{ij}$ is not a constant, but
must be calculated for \textit{every} interaction in \textit{each}
time step.

Tersoff\cite{tersoff1988new} proposed the following form
\begin{align}
  \tilde{b}_{ij} &= \left( 1 + \beta^n \zeta^n_{ij}
  \right)^{ - \frac{1}{2n}}, \nonumber \\
  \label{eq:9}
  \zeta_{ij} &= \sum_{k \neq i,j} f_C (r_{ik} ) ~ g_C(\vartheta_{ijk}
  ) ~\exp \left[ \lambda_3^3 (r_{ij} - r_{ik} )^3
  \right], \\
  g_c(\vartheta) &= 1 + \frac{c^2}{d^2} - \frac{c^2}{d^2 + (h - \cos
    \vartheta )^2 } , \nonumber
\end{align}
where $\vartheta_{ijk}$ is the angle between $\vec r_{ij}$ and $\vec
r_{ik}$.  The parameters $c$, $d$, $h$, $\beta$, $n$ and $\lambda_3$
are used in the fitting procedure, where they are chosen such that the
resulting potential can reproduce known properties like stable
structures or phonon energies.  It should be noted, that
$\tilde{b}_{ij} \neq \tilde{b}_{ji}$ due to different environments
around $i$ and $j$.  As a consequence binding energies can be
non-uniform between two atoms.  This bond-order term can be further
modified, as will be shown in Sec. \ref{sec:react-empir-bond}.

The Tersoff potential consists of an attractive $V_A$ and a repulsive
part $V_R$, which are modified by the functions $f_C$ and
$\tilde{b}_{ij}$.  Following Abell these functions are chosen with exponential decay:
\begin{subequations}
  \begin{align}
    \label{eq:5}
    V_{R} (r_{ij}) & = f_C(r_{ij}) \, A \exp \left( - \lambda_1 r_{ij} \right), \\
    \label{eq:6}
    V_{A} (r_{ij}) & = - f_C(r_{ij}) \, B \, \exp \left( - \lambda_2
      r_{ij} \right) \, .
  \end{align}
\end{subequations}

The interactions are mostly acting on a short-range. Computational time
can be saved if less interactions must be calculated. Only the nearest
neighbors like the first shell of neighbors are considered.  This is
achieved via a \textit{cut-off function} $f_C$ like
\begin{align}
  \label{eq:7}
  f_{C} (r_{ij}) &= \Theta\left(r_{ij}^{\mathtt{min}} - r_{ij}\right)
  + \Theta\left(r_{ij} - r_{ij}^{\mathtt{min}}\right)
  \Theta\left(r_{ij}^{\mathtt{max}} - r_{ij}\right) \frac{1}{2}\left[
    1+ \cos \left( \pi \frac{r_{ij} - r^{ \mathtt{min}}_{ij}}{r^{
          \mathtt{max}}_{ij} - r^{\mathtt{min}}_{ij}} \right)\right]
  % \begin{dcases}
  %   1&,~ r_{ij} < R^{(1)}\\
  %   \frac{1}{2} - \frac{1}{2} \sin \Bigg[ \frac{\pi}{2} \cdot
  %   \left(\frac{2 \cdot r - R^{(1)} - R^{(2)}}{R^{(2)} - R^{(1)}}
  %   \right)\Bigg]
  %   &,~ R^{(1)} < r_{ij} < R^{(2)}\\
  %   0&,~ r_{ij} > R^{(2)}.
  % \end{dcases}
\end{align}
which smooths the potential to zero in the transition region from
$r_{ij}^{\mathtt{min}}$ to $r_{ij}^{\mathtt{max}}$ leading to no
further interactions between these atoms further apart.  In the
original Tersoff ansatz this region was uniform for all different
interactions.  As will be seen in the following section
\ref{sec:react-empir-bond}, this cut-off region can be dependent on the
interacting species. This is indicated by the $_{ij}$ index.  The
cut-off distance depends on the species of atom $i$ and $j$.

Unlike classical force-fields, where bounds are approximated
with harmonic oscillators resulting in very large walls on both sides,
the smoothing term combined with the decay functions \eqref{eq:5} and
\eqref{eq:6} enables breaking and forming of bounds as long as
sufficient kinetic energy is available.  Consequently, the depth of
the potential is fitted to known bond-dissociation energies.  This
does not mean a Tersoff-like potential is always superior to a
classical force-field, because the inclusion of exponential and cosine
terms is computationally much more costly but also as the neighbors of
an atom can change, regular checks of the neighbor list are necessary
(see chapter \ref{sec:numer-impl} for further details).  For studies
of large molecules, where no bonds are formed or broken, classical
force-fields are the right choice (e.g. for non-reactive
fluorine carbon systems\cite{doi:10.1021/ma025645t}).

In the form of $a_{ij}$ proposed by Tersoff\cite{tersoff1988new} this
term is only different from $\simeq 1$ outside the first shell of
neighbors, which is excluded by the cut-off function $f_C(r_{ij})$.
Therefore, in most applications of the Tersoff-potential $a_{ij}$ it
is assumed as $1$ and is not further considered.

% Using a Tersoff potential with only the nearest neighbors leads to a
% system recognizing only \textit{intramolecular} interactions. This is
% often ill-suited for the analysis of a system mostly consisting of
% molecules rather than a bulk material, like melts or amorphous
% surfaces.  Also the original potential parameters are optimized for
% pure silicon bulk materials.
% % \todo{unvollständiger Satz parameters... carbons}
% A simple extension to gain a set of parameters for carbon could not be
% obtained with basic Tersoff-potentials. The complex behavior of
% radicals or the $\pi$-binding states are not well qualified.

The overbinding of radicals may occur in the Tersoff potential, e.g.
the bonds to an atom with low coordination number, say three for a
carbon atom, will be interpolated between single and double bonds.
Free $2p$ orbitals at the ligand are not considered because in the
Tersoff-potential no difference exists between single, double
and triple bonds. %\todo{das mal nen satz :) gehts auch kürzer?}
Likewise conjugated and non-conjugated double bonds are treated the
same, although they will have different binding energies.

% carbon molecules are the subject of interest, so that an
% interaction potential between the different species of these molecules
% must be defined. It is therefore necessary to make changes to the
% bond-order function, add further species in the fitting procedure and
% add a further potential to include an \textit{intermolecular}
% interaction.

For hydrogen-carbon systems an extension of Tersoff potential was
suggested by Brenner in 1990\cite{brenner1990empirical} through the
\textit{reactive empirical bond-order potential} (REBO-potential)
which will be specified in following.

%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t
%%% TeX-master: "../main"
%%% End: 
