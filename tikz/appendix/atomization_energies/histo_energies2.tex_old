% \documentclass[10pt,landscape]{article}

% \usepackage{pgfplots}
% \pgfplotsset{compat=newest}% <-- moves axis labels near ticklabels (respects tick label widths)

% \begin{document}
% \thispagestyle{empty}

\begin{tikzpicture}

  \begin{axis}[
        xbar,
    height=22cm,
    width=14cm,
    bar width=0.3cm,
    enlarge x limits=false,    
    enlarge y limits={upper,value=0.05},
    enlarge x limits={upper,0.05},
    enlarge x limits={lower,0.03},
    axis lines*=left,
    xmin=0,
    xmax=100,
    legend style={at={(0.5,-0.15)},
      anchor=north,legend columns=-1},
    xlabel={atomization energies in eV},
    symbolic y coords={\ce{C_2F_4}, 
      \ce{C_2F_6}, 
      \ce{C_2H_2}, 
      \ce{C_2H_2F_2}, 
      \ce{C_2H_4},  
      \ce{C_2H_4F_2},  
      \ce{C_2H_6}, 
      \ce{C_3F_8},  
      \ce{C_3H_8}, 
      \ce{C_6F_{12}}, 
      \ce{C_6F_6}, 
      \ce{CF_3}, 
      \ce{CF_4}, 
      \ce{CH_2F}, 
      \ce{CH_2F_2}, 
      \ce{CH_3F},  
      \ce{CH_4}, 
      \ce{CHF_2}, 
      \ce{CHF_2CHF_2},  
      \ce{CHF_3}},
    %symbolic y coords={$C_2F_2$, $C_2F_4$, $C_2F_6$, $C_2H_2$, $C_2H_2F_2$, $C_2H_4$, $C_2H_4F_2$, $C_2H_6$, $C_3F_8$, $C_3H_8$, $C_6F_{12}$, $C_6F_6$, $CF_3$, $CF_4$, $CH_2F$, $CH_2F_2$, $CH_3F$, $CH_4$, $CHF_2$, $CHF_2CHF_2$, $CHF_3$},
    % symbolic x coords={mod2,mod3,mod5,mod7,mod11,mod13,mod17,mod19,mod23},
    ytick=data, 
    nodes near coords, 
    %yticklabel style={ inner sep=0pt, anchor=east, rotate=90 }, 
    %every node near coord/.append style={anchor=mid west, rotate=90
    %}, 
    ]
    % \addplot coordinates{($C_2F_2$,1) ($C_2F_4$,2)};
    \addplot coordinates{ (18.15,\ce{C_2F_2})
      (26.26,\ce{C_2F_4})
      (37.97,\ce{C_2F_6})
      (17.57,\ce{C_2H_2})
      (25.39,\ce{C_2H_2F_2})
      (24.53,\ce{C_2H_4}) 
      (33.23,\ce{C_2H_4F_2}) 
      (30.85,\ce{C_2H_6})
      (52.52,\ce{C_3F_8}) 
      (43.59,\ce{C_3H_8})
      (87.27,\ce{C_6F_{12}})
      (58.9,\ce{C_6F_6})
      (13.81,\ce{CF_3})
      (23.01,\ce{CF_4})
      (13.54,\ce{CH_2F})
      (20.71,\ce{CH_2F_2})
      (19.44,\ce{CH_3F}) 
      (18.19,\ce{CH_4})
      (13.68,\ce{CHF_2})
      (25.22,\ce{CHF_2CHF_2}) 
      (21.9,\ce{CHF_3}) }; 

    \addplot coordinates{
      (18.27,\ce{C_2F_2}) 
      (25.89,\ce{C_2F_4}) 
      (33.07,\ce{C_2F_6})
      (17.57,\ce{C_2H_2})
      (25.2,\ce{C_2H_2F_2})
      (24.4,\ce{C_2H_4})
      (31.56,\ce{C_2H_4F_2}) 
      (30.85,\ce{C_2H_6})
      (46.21,\ce{C_3F_8})
      (43.59,\ce{C_3H_8})
      (77.74,\ce{C_6F_{12}})
      (59.44,\ce{C_6F_6})
      (14.7,\ce{CF_3})
      (19.81,\ce{CF_4})
      (13.84,\ce{CH_2F})
      (19.08,\ce{CH_2F_2})
      (18.63,\ce{CH_3F})
      (18.19,\ce{CH_4})
      (14.28,\ce{CHF_2})
      (32.53,\ce{CHF_2CHF_2})
      (19.48,\ce{CHF_3}) };
    % \addplot table [col sep=comma, header=false] {data2.dat};
    % \addplot coordinates {(mod2,75.4064) (mod3,89.7961)
    % (mod5,94.4597)
    % (mod7,96.6786) (mod11,97.5600) (mod13,98.2339)
    % (mod17,98.6138) (mod19,98.9129) (mod23,99.0970)};
    % \addplot coordinates {(mod2,30.5101) (mod3,34.5384)
    % (mod5,36.3324)
    % (mod7,37.3570) (mod11,37.9158) (mod13,38.3514)
    % (mod17,38.6484) (mod19,38.9125) (mod23,39.1067)};
    \legend{our implementation,Jang/Sinott}
  \end{axis}
\end{tikzpicture}

% \end{document}
