function stick_to_angle ()

  ## usage:  stick_to_angle ()
  ##
  ## 

  a1 = 1.015
  b1 = 1.798
  c1 = 0.980
  d1 =-0.043

  S = @(x) a1 .* tanh(b1.*cos(c1.*x)) .+ d1

  figure(1);

  fplot(S,[0 pi/2.0])

  hold on
  a2 = -.17
  b2 = 7.989e-2
  c2 = -2.209e-4
  d2 = 0.929

  F1 = @(x)1./(1.+exp(-a2.-b2.*x)).*d2 ./(1.+c2.*x)

  a = (0.01-F1(5))/(-log(5)+log(0.03))
  b = F1(5)-a*log(5)

  F2 =@(x)a.*log(x).+b


  F = @(x)heaviside(5-x).*F2(x).+heaviside(x-5).*F1(x)
  G = @(x)F1(x)

  dx=0.03:0.01:100;

  dataX=F(dx);

  #semilogx(dx,data)

  SE = @(x)F(50.*cos(x).^2)  
  SEE = @(x)G(50.*cos(x).^2)  
  #SEEE = @(x)abs(S(x) .- F(50.*cos(x).^2)) 
  

  fplot(SE,[0 pi/2.0 0 1], 'r') 
  fplot(SEE,[0 pi/2.0 0 1], 'g')
  #fplot(SEEE,[0 pi/2.0 0 1], 'c')
  hold off

  # compare sticking coefficients

  da = 0:0.01:pi/2.0;
  dE = 50*cos(da).^2;
  data = S(da);

  [dE,idE] = sort(dE);
  data(idE) = data(:);
  figure(2);
  semilogx(dx,dataX,'r',dE,data,'b')

  outfile = "angle_fit.dat";
  fd = fopen(outfile, "wt");
  
  fprintf(fd, "%14.8g %14.8g\n",reshape([da ; S(da)],2, length(da)));
  fclose(fd);

  outfile = "angle_calc.dat";
  fd = fopen(outfile, "wt");
  
  fprintf(fd, "%14.8g %14.8g\n",reshape([da ; SE(da)],2, length(da)));
  fclose(fd);

  outfile = "energy_fit.dat";
  fd = fopen(outfile, "wt");
  
  fprintf(fd, "%14.8g %14.8g\n",reshape([dx ; dataX],2, length(dx)));
  fclose(fd);

  outfile = "energy_calc.dat";
  fd = fopen(outfile, "wt");
  
  fprintf(fd, "%14.8g %14.8g\n",reshape([dE ; data],2, length(dE)));
  fclose(fd);

endfunction