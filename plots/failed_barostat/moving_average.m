function moving_average ()

  ## usage:  moving_average ()
  ##
  ## 

  A = load("-ascii","evil4.txt");

  size(A)

  step_size=1.0

  dp1 = 1:step_size:140000;
  dp2 = 10001:2:140001;


  X1 = A(dp1,1);


  #t = (0:.001:1)';                                %#'
  #vector = sin(2*pi*t) + 0.2*randn(size(t));      %# time series

  wndw = 10000/step_size;                                      %# sliding window size
  #output1 = filter(ones(wndw,1)/wndw, 1.0, vector); %# moving average
  temp = filter(ones(wndw,1)/wndw, 1.0, A(dp1,5)-A(dp1,3)); %# moving average
  pair =  filter(ones(wndw,1)/wndw, 1.0, A(dp1,3));
  eng = filter(ones(wndw,1)/wndw, 1.0, A(dp1,5)); %# moving average
  press = filter(ones(wndw,1)/wndw, 1.0, A(dp1,6)./1000); %# moving average
  vol = filter(ones(wndw,1)/wndw, 1.0, A(dp1,7)); %# moving average

  #plot(X,output1,X(wndw:end),detrend(output1(wndw:end),3));
  plot(X1(wndw:end),temp(wndw:end)-temp(wndw),'r',\
       X1(wndw:end),eng(wndw:end)-eng(wndw),'g',\
       X1(wndw:end),pair(wndw:end)-pair(wndw),'b',\
       X1(wndw:end),vol(wndw:end)-vol(wndw),'c',\
       X1(wndw:end),press(wndw:end)-press(wndw),'y');

  dx=X1(wndw:end)';
  a=size(dx)(2)
  dataX=[temp(wndw:end)'-temp(wndw);\
	 eng(wndw:end)'-eng(wndw);\
	 pair(wndw:end)'-pair(wndw);\
	 vol(wndw:end)'-vol(wndw);\
	 press(wndw:end)'-press(wndw);\
	 A(1:a,7)'];

  B = reshape([(dx-1790998+5000+1)/1E4; dataX/1E3],7,length(dx));

  B (:,1:3)

  size(dataX)

  outfile = "means.dat"
  fd = fopen(outfile, "wt");
  
  fprintf(fd, "%14.8g %14.8g %14.8g %14.8g %14.8g %14.8g %14.8g\n", \
	   B );
  fclose(fd);

  (temp(wndw)+temp(wndw+1))*0.5
  (eng(wndw)+eng(wndw+1))*0.5
  (pair(wndw)+pair(wndw+1))*0.5
  (vol(wndw)+vol(wndw+1))*0.5
  (press(wndw)+press(wndw+1))*0.5
  
  #max(press(wndw:end))

  
endfunction