function fft_ana ()

  ## usage:  fft_ana ()
  ##
  ## 

  A = load("-ascii","evil4.txt");

  size(A)

  dp = 10000:2:60001;
  dp2 = 10001:2:60001;

  B = A(dp,7);

  X = A(dp,1);
  #Y = A(dp,5) .- 2.*A(dp,3);
  #Y = (A(dp,3).+A(dp2,3)).*0.5;

  strangef = @(x)(cos(pi./34.*x) .+ cos(pi./47.*x).^2);

  #Y .-= mean(Y);

  #X = X .* 1E-4;

  X .-= X(1);
  Y = detrend((A(dp,2).+A(dp2,2)).*0.5,3);
  wndw = 10;                                      # sliding window size
  Y2 = filter(ones(wndw,1)/wndw, 1, Y); # moving average

  #Y = detrend(A(dp,2),3);
  ##Y = detrend(strangef(X),3);

  X .*= 1E-4;


  dx = X(2) -X(1);

  #XI = X(1):dx/10:X(end);
  #YI = interp1(X,Y,XI,'spline');


  #X =XI;
  #Y = YI;
  L = length(X);
  dx = X(2) -X(1);

  dx = 1e-3

  Fs = 1./dx;
  
  NFFT = 2^nextpow2(L);
  f = Fs/2*linspace(0,1,NFFT/2+1);

  p = polyfit(X,Y,1);

  a = 0.0291462;
  b = 0.208568;
  c = 1.11401;

  expf = @(x)1E6*(a*exp(b*x)+c);

  u = -7.6431e-07;
  v = 1.19423e-05;

  j = 5515.75;
  i = -10044.4;
  l = 0.18180;
  k = 14.3795;
  
  hf = @(x)j./(k.-x).^l.+i

  gf = @(x)1.0./(u.*x.+v).^(1.0/1.5);

  
  [b] = blackman(100);
  Y2 = filter2(b,Y);

  E = fft(Y,NFFT);
  #E2 = fft(Y2,NFFT);
  #E = fft(Y-polyval(p,X),NFFT);
  #E = fft(Y-hf(X),NFFT);
  #E = fft(Y-expf(X),NFFT);

  length(f)
  figure(1);
  plot(X,Y,X,polyval(p,X),X,detrend(Y,3))
  #plot(X,Y,X,polyval(p,X))

  figure(2);
  #plot(f(1:end),2*abs(E(1:NFFT/2+1)),f(1:end),2*abs(E2(1:NFFT/2+1)))
  plot(f(1:end),2*abs(E(1:NFFT/2+1)))
  
  outfile = "dft.dat"
  fd = fopen(outfile, "wt");
  fprintf(fd, "%14.8g %14.8g\n", reshape([f;2*abs(E(1:NFFT/2+1)')],2,length(f)));
  fclose(fd);

  abs(E(1:10))

  corrY = autocor(Y,NFFT/2+1);

  size(corrY)

  figure(3);
  plot(0:1:NFFT/2+1,corrY)

  outfile = "corr.dat"
  fd = fopen(outfile, "wt");
  fprintf(fd, "%14.8g %14.8g\n", reshape([(0:1:NFFT/2+1);corrY'],2,NFFT/2+2));
  fclose(fd);


  #figure(4);
  #ymax=max(Y(1:5000));

  #scatter(Y(1:5000-25)./ymax,Y(26:5000)./ymax)

  figure(4);
 
  plot (X,Y,X,Y2);

endfunction