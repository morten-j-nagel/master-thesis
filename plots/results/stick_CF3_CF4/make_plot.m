function make_plot ()

  ## usage:  make_plot ()
  ##
  ## 

  load data_CF3.dat

  data

  #Y = zeros(length(data)/2,3);

  iLJ=iR=0;

  for i=1:length(data)
    if data(i).lj
      iLJ++;
      XLJ(iLJ) = data(i).energy;
				#for j=1:3
      YLJ(iLJ,:) = \
	  [data(i).reactions{1,3},data(i).reactions{1,4}];
    else
      iR++;
      X(iR) = data(i).energy;
				#for j=1:3
      Y(iR,:) = \
	  [data(i).reactions{1,3},data(i).reactions{1,4}];
    endif
  endfor

  #X',Y(:,1:2), YLJ
  semilogx(X,Y(:,1),XLJ,YLJ(:,1))

  outfile = strcat("plot_CF3.dat","");
  fd = fopen(outfile, "wt");


  fprintf(fd, "%14.8g %14.8g %14.8g %14.8g %14.8g\n", \
  	  reshape([X',Y(1:length(X),1:2), YLJ(1:length(X),1:2)], length(X),5)');
  fclose(fd);
endfunction