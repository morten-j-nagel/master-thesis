function ana_dist (filename,plot_dir)

  ## usage:  ana_dist ()
  ##
  ## 

  #plot_dir="plots/"
  colors = ["y","r","m","b","c","g"];
  set(0, "defaultfigurevisible", "off");
  filename

  #break;

  x1 = load("-ascii", "wayC1.dat");
  x2 = load("-ascii", "wayC2.dat");

  #force = x3(:,8);

  d(:,1) = x2 (:,4) - x1(:,4);
  d(:,2) = x2 (:,5) - x1(:,5);
  f(:,1) = x1 (:,11);
  f(:,2) = x1 (:,12);
  t = 0:0.0001:length(x1);
  dist = sqrt(d(:,1).^2 + d(:,2).^2);
  force = sqrt(f(:,1).^2 + f(:,2).^2);

  size(d)

  i = 1;
  start = 0;
  while ( i<size(d)(1) )
    
    if (force(i) > 0) && (start == 0)
      start = i; 
    elseif (start != 0) && (force(i) == 0)
      break;
    endif

    i++;
  endwhile
  stop = i-2;

  k = 1;
  turns(k++) = start;
  for j = start+1:stop-1
    if (dist(j)<dist(j-1)) && (dist(j) < dist(j+1))
      turns(k++) = j;
    endif
  endfor

  turns(k++)=stop
  start
  stop

  tenergy = (x1(:,7).*2 + x1(:,10) +  x2(:,10));
  n_tenergy = min(tenergy);
  figure(1);
  plot (dist(start:stop), x1(start:stop,7),"+", \
	dist(start:stop), x1(start:stop,10),"+", \
	dist(start:stop), x2(start:stop,10),"+")
  name = strcat(plot_dir,"energy_",filename,".png")
  print (name,"-dpng");


  figure(2);
  plot (dist(start:stop), tenergy(start:stop), "r")
  name = strcat(plot_dir,"dist_energy_",filename,".png")
  print (name,"-dpng");

  figure(3);
  plot (dist(start:stop), force(start:stop), "b+"); 
  name = strcat(plot_dir,"dist_force_",filename,".png")
  print (name,"-dpng");

  figure(4);
  plot(dist(stop:end), force(stop:end));
  name = strcat(plot_dir,"dist_force_end_",filename,".png")
  print (name,"-dpng");
  figure(5)
  hold off
  hold on
  for l = 1:length(turns)-1
    m = rem(l,length(colors))+1;
    outfile = strcat("energy_old_",int2str(l),"_plot.dat");
    fd = fopen(outfile, "wt");
    
    fprintf(fd, "%14.8g %14.8g\n", \
	    reshape([dist(turns(l):turns(l+1)), \
		     1000.*tenergy(turns(l):turns(l+1))]',\
		    1, 2*(turns(l+1)-turns(l)+1)));
    fclose(fd);
    plot(dist(turns(l):turns(l+1)), tenergy(turns(l):turns(l+1)),colors(m))
  endfor
  name = strcat(plot_dir,"dist_energy_color_",filename,".png")
  print (name,"-dpng");

  figure(6);
  plot (dist(start:stop), 2.*x1(start:stop,7) .+ x1(start:stop,10) .+ \
	x2(start:stop,10),"+")
  name = strcat(plot_dir,"sum_energy_",filename,".tex")
  print (name,"-dtikz");


  outfile = strcat("data_old_",int2str(1),"_plot.dat");
  fd = fopen(outfile, "wt");

  fprintf(fd, "%14.8g %14.8g %14.8g %14.8g %14.8g\n", \
	  reshape([t(start:stop)'.*1E3, x1(start:stop,7), \
		   x1(start:stop,10), x2(start:stop,10), \
		   dist(start:stop)]', 1, 5*(1+stop-start)));
  fclose(fd);


  [valx,ix] = min(x1(start:stop,7))
  
  dist(start:stop)(ix)

endfunction
