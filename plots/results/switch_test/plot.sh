#!/bin/bash

filename="DUMP.lmp_imint+CHF.rebo+inFILES_in.CC+vel$1+t$2_old"
script_dir=$(pwd)
plot_dir="plots9/"
mkdir $plot_dir -p
cat $filename | grep "1 12 "> wayC1.dat
cat $filename | grep "3 12 "> wayC2.dat

gnuplot <<EOF
set term png size 800,800
set out '${plot_dir}vel_$1_t_$2.png'
#set xr [4.9:5.2]
set grid
unset key
a=1
b=1
c=2000
plot 'wayC1.dat' u 4:5 ev a::b::c, 'wayC2.dat' u 4:5 ev a::b::c
#pause -1
EOF

octave -q  --eval "ana_dist (\"vel$1+t$2\",\"${plot_dir}\")"
