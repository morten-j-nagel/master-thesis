if (!exists("GPFUN_get_path")) get_path(file) = sprintf("./%s",file)

set key box at screen 1,0.5 right center vertical Left reverse \
spacing 8.5 width -58 height 1.5 maxcols 1 maxrows 6

set xlabel "$r_{ij}$ / \\si{\\angstrom}"
set ylabel "$\\Phi_{ij}\\mapar{r_{ij}}$ / \\si{\\eV}" 

set terminal epslatex color colortext lw 2 size 6.5,4
set out get_path("basic_interaction.tex")

set yr [-7:4]
set xr [0.8:2]

set grid

set rmargin 23


set ytics 2

set xzeroaxis lt 1 lc 0 lw 3

set parametric
set tr [0:1]
x(t) = 0.8 + 1.2*t

set label 1 "cut-off region" at 1.85,4.5 center tc rgb "orange"

set obj 1 rectangle behind from 1.7,-7 to 2,4 \
lw 1 fc rgb "orange" fillstyle transparent pattern 5 noborder

get_title(a,b,c) = sprintf("$\\left\\{\\begin{minipage}[H]{5.0cm}$\\Phi^{%s}$ w/ \\\\$r^{%s}_{eq.} = \\SI{%.3g}{\\angstrom}$, \\\\$\\Phi^{%s}_{min.} = \\SI{%.3g}{\\eV}$ \\end{minipage}\\right.$",a,a,b,a,c)

get_t_dash(a) = sprintf("$\\Phi^{%s}$ w/o $f_C\\mapar{r}$",a)

plot get_path("data_CC.dat") u 1:2 w l lt 3 lc 1 lw 3 t "",\
     get_path("data_CF.dat") u 1:2 w l lt 3 lc 3 lw 3 t "",\
     get_path("data_FF.dat") u 1:2 w l lt 3 lc 2 lw 3 t "",\
     get_path("data_CC.dat") u 1:3 w l lt 1 lw 3 lc 1 t "",\
     get_path("data_CF.dat") u 1:3 w l lt 1 lw 3 lc 3 t "",\
     get_path("data_FF.dat") u 1:3 w l lt 1 lw 3 lc 2 t "",\
     x(t),-x(t)*100 w l lt 1 lw 3 lc 1 t get_title("CC",1.33,-6.21),\
     x(t),-x(t)*100 w l lt 3 lw 3 lc 1 t get_t_dash("CC"),\
     x(t),-x(t)*100 w l lt 1 lw 3 lc 2 t get_title("FF",1.41,-1.51),\
     x(t),-x(t)*100 w l lt 3 lw 3 lc 2 t get_t_dash("FF"),\
     x(t),-x(t)*100 w l lt 1 lw 3 lc 3 t get_title("CF",1.27,-5.71),\
     x(t),-x(t)*100 w l lt 3 lw 3 lc 3 t get_t_dash("CF"),\
     1.33,-6.21 -2 +4*t t '' lt 4 lc 0,\
     1.33 -.2 +.4*t,-6.21 t '' lt 4 lc 0,\
     1.41,-1.51 -2 +4*t t '' lt 4 lc 0 ,\
     1.41 -.2 +.4*t,-1.51 t '' lt 4 lc 0 ,\
     1.27,-5.71 -2 +4*t t '' lt 4 lc 0,\
     1.27 -.2 +.4*t,-5.71 t '' lt 4 lc 0
     

unset out

#pause -1