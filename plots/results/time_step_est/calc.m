function calc ()

  ## usage:  calc ()
  ##
  ## 

  Q = 0.313460
  A = 10953.54
  B1 = 12388.79198
  B2 = 17.567
  B3 = 30.714832
  a = 4.746539
  b1= 4.720452
  b2= 1.433212
  b3= 1.382691

  # Q = 0
  # A = 909.2022
  # B1 = 219.7799
  # B2 = 0
  # B3 = 0
  # a = 3.7128
  # b1= 2.1763
  # b2= 0
  # b3= 0

  # Q = 0
  # A = 16451.91
  # B1 = 146.8149
  # B2 = 0
  # B3 = 0
  # a = 6.8149
  # b1= 2.8568
  # b2= 0
  # b3= 0
	 
  global dmin
  global dmax
  dmin = 1.5
  dmax = 2.0
  f = @(x)( (1.+Q./x).*A.*exp(-a.*x) .- B1.*exp(-b1.*x) .- \
	   B2.*exp(-b2.*x) .- B3.*exp(-b3.*x))

  f1 = @(x) -A.*(a .+ a*Q./x + Q./x.^2).*exp(-a.*x) \
  .+ B1*b1.*exp(-b1.*x) .+ B2*b2.*exp(-b2.*x) .+ \
      B3*b3.*exp(-b3.*x)

  f2 =@(x) A.*(a^2 .+ a^2*Q./x + 2*a*Q./x.^2 + 2*Q./x.^3).*exp(-a.*x)\
      .- B1*b1^2.*exp(-b1.*x) .- B2*b2^2.*exp(-b2.*x) .- B3*b3^2.*exp(-b3.*x)
  
  grid on

  dx = 0.8:0.001:2;
  
  for i=1:length(dx)
    dsf(i) = s(dx(i))*f2(dx(i));
    df(i) = f2(dx(i))*f(dx(i));
  endfor

  dmin = 1.7
  for i=1:length(dx)
    dsf2(i) = s(dx(i))*f2(dx(i));
  endfor
  

  plot(dx,df)

  2*pi*sqrt(24/f2(1.32))

  outfile = strcat("data_dev2.dat","");
  fd = fopen(outfile, "wt");

  fprintf(fd, "%14.8g %14.8g\n", \
    	  reshape([dx;df], 2, length(dx)));
  fclose(fd);

  
#reshape([dx;df;dsf], 3, length(dx))(:,1:5)
endfunction


function y = s(x)
  global dmin
  global dmax

  if ((x>dmin) && (x < dmax))
   y = 0.5.*(1.+cos(pi.*(x.-dmin)./(dmax-dmin)));
  elseif (x >= dmax)
    y = 0;
  elseif (x <= dmin)
    y = 1;
  endif
  
endfunction 
