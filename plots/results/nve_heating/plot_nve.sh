#!/bin/bash

#  generate datfiles with
# grep "\-[0-9][0-9][0-9]" < logs/runtest/log.inFILES_in.nve_test_0.0

data_file=$1

echo ${data_file//\./\_}_temp.png

echo "a=1
b=1
c=1
d=1
e=1
f=1
u=1
v=1
">${data_file##\.txt}"_fit.txt"

gnuplot -persist <<EOF
unset key
set term png size 1440,800
set xlabel "t / ps"
set ylabel "T / K"
#set format x "%.1e"

# fitting

f(x) = a*x + b

FIT_LIMIT = 1e-10

fit f(x) "${data_file}" u (\$1/1E4):2 via a,b

set out "${data_file//\./\_}_temp.png"

plot "${data_file}" u (\$1/1E4):2 w p, f(x) w l lw 3

set out "${data_file//\./\_}_temp_var.png"

plot "${data_file}" u (\$1/1E4):(\$2 - f(\$1/1e4)) w p

set ylabel "E / keV"

set table
set out "kin_eng.dat"
plot "${data_file}" u (\$1/1E4):(\$5-\$3)
unset table

set out "${data_file//\./\_}_total.png"

g(x) = c*x + d
h(x) = e*x + f
l(x) = u*x + v

fit g(x) "${data_file}" u (\$1/1E4):(\$5) via c,d
fit h(x) "${data_file}" u (\$1/1E4):(\$3) via e,f
fit l(x) "kin_eng.dat" u 1:2 via u,v

update "${data_file##\.txt}_fit.txt"

plot "${data_file}" u (\$1/1E4):(\$5) w p, g(x) w l lw 3

set out "${data_file//\./\_}_kin.png"

plot "kin_eng.dat" u 1:2, l(x) w l lw 3

set out "${data_file//\./\_}_total_pot.png"

plot "${data_file}" u (\$1/1E4):(\$5) w p, "" u (\$1/1E4):(\$3) w p,\
 g(x) w l lw 3, h(x) w l lw 3

set table
set out "time_total.dat"
plot "${data_file}" u (\$1/1E4):(\$5/1E3)
unset table
EOF
