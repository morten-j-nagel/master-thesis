\chapter{Motivation}
\label{sec:Motivation}

Fluorocarbon plasmas are used for reactive etching in industry to
build computer chips.  Deposited amorphous fluorine carbon films and
polymers have quite favourable properties.  These are their relative
high thermal stability of up to
\SI{530}{\kelvin}\cite{dupontproperties}.  Secondly, they have an
ultra low surface tension due to weak polarizability of the contained
fluorine atoms.  These effects are commonly used in products like
GORE-TEX\cite{gore1976process}.  Thirdly, fluorocarbon films have also
a low permittivity, therefore they are called a
\textit{low-k-material}.  These are getting more and more interesting
for the computer chip manufacturing industry as a way to reduce
parasitic capacitances in smaller structures.  Fourthly, fluorocarbon
films have due to their \textit{weak London dispersion force} a very
low coefficient of friction \cite{doi:10.1021/jo0302556}.  As
fluorocarbons are a major persistent green house gas their optimized
usage should be achieved\cite{Ravishankara08011993}.

Up to now, development and research follow a trial-and-error strategy,
because experimental characterization and existing models are both
rather incomplete.  Plasma parameters are only partly known, e.g. the
density of radicals is measured but not their energy distribution.  In
modeling, there is a lack of integrated models of such plasmas,
including plasma-wall interaction processes.  Such incomplete models
miss any predictive capability, because they do not resolve the
fundamental non-linear characteristics of such systems.  Plasma fluxes
onto the walls determine both film properties and erosion or
deposition dynamics through their angular, energetic and species
distributions.  The wall responses in terms of back-scattering,
sticking or sputtering provide sinks and sources for the plasma
dominating the flux balances.  The existence of a wall in front of a
plasma creates characteristic non-linear boundary effects on the
plasma flows, producing a boundary potential reflecting most of the
electrons and accelerating ions.

Even in a simplified, 0D integrated model developed by Gunnar
Bandelow, similar to \cite{PPAP:PPAP201000095}, the influence of
plasma wall processes in terms of sticking coefficients on the results
is quite strong as a sensitivity study shows.  Additional sinks act
through the non-zero sticking coefficients, therefore the system get a
new equilibrium with densities different from a pure bulk situation.
Usually, sticking coefficents are not well-known and only estimates
exist.  Results are shown in Figure~\ref{fig:stick_start} where a
reference case in red is plotted with varied sticking (other colors).
By this the reference sticking coefficients of \ce{CF3}
($\beta_{\cee{CF3}} = 0.1$), \ce{CF2} ($\beta_{\cee{CF2}} = 0.15$) and
\ce{CF} ($\beta_{\cee{CF}} = 0.2$) are varied with a scaling factor.
The coefficients of \ce{F} and \ce{C} are kept constant with $\beta =
0.25$.  This means that the loss channels are amplified for the three
varied species.  All observed species in the global model show changes
in their density but in Figure~\ref{fig:stick_start} only three
examples are shown.  The density of the feed-gas \ce{CF4} seems
relative unaffected by the scaling factor for the sticking
coefficients.  But because it is the species with the largest density
in the system changes of about a percent will cause big changes to
other species.  Interestingly with higher sticking coefficients the
density for \ce{CF4} \textit{increases} slightly.  The same trend
appears more pronounced for \ce{F2} which density is more than doubled
in this variation study.  The effect of higher sticking coefficients
is contrary on the density of \ce{C2F4}, which is strongly reduced.
More details on this sensitivity study are discussed in detail in
section~\ref{sec:infl-stick-coeff} using the sticking coefficients
obtained in this work.

\begin{figure}[htb]
  \captionsetup{indent=-1.0cm}
  \centering
  \includegraphics{tikz/histos/sensitive-crop}
  \caption{Influence of sticking on the density of selected species in
    the global model.  The numbers in the legend are the scaling
    factors for an assumed stick of $\beta_{\cee{CF3}} = 0.1$,
    $\beta_{\cee{CF2}} = 0.15$ and $\beta_{\cee{CF}} = 0.2$. The
    sticking coefficients $\beta_{\cee{F}} = 0.25$ and
    $\beta_{\cee{C}} = 0.25$ are constant.}
  \label{fig:stick_start}
\end{figure}

The main topic of this work is the computational investigation of
plasma wall interaction in \textit{carbon fluorine plasmas}.  This is
done by using the method of \textit{Molecular Dynamics}.
Specifically, sticking coefficients of molecules will be calculated
and their influence on the global characteristics of such plasmas will
be discussed.

The structure of the thesis is as follows.  At first the basic
properties of carbon fluorine plasmas and films are given.
Afterwards, an introduction to the computational method of Molecular
Dynamics is presented, followed by a detailed discussion of the
interaction potentials used, which include all information about the
system and determine its properties.  The specific code used in this
work, LAMMPS, and own modifications and extensions in the numerical
implementation are introduced.  The validation strategy is discussed
and results are presented on the creation of samples and sticking
coefficients.  The influence on these coefficients on the global,
intergrated model results will be shown.  At last the work is
summarized and as an outlook possible extensions are given.


%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t
%%% TeX-master: "../main"
%%% End: 


% \chapter{Motivation}
% \label{sec:motiv}

% For?
% \begin{itemize}
% \item definition fluorocarbon
% \item CF4 as etching gas in industry silicone to build computer
%   chips
% \item example products GORETEX, pfannen beschichtung
% \item favored properties of such films ( ultra low surface tension,
%   low friction low k-permittivity, thermal stable region, chemical
%   inertness )
% \item (different experiments techniques plasmas, hot wire, ..)

% \end{itemize}

% Why?
% \begin{itemize}
% \item experimental evidence (creation of \ce{SiF4} in flask
%   containing a fluorine carbon gas and creation of too much
%   \ce{C2F4} in the off phase
% \item description of the global model
%   \begin{itemize}
%   \item balance of densities
%   \item reactions of the species in the plasma
%   \item possible loss to and gains from the surface?
%   \end{itemize}
% \item introduction ``sticking coefficient'' as a loss channel
% \item influence of sticking coefficients in the global model
%   (description below)
% \end{itemize}

% What?
% \begin{itemize}
% \item theme of the work (title to first sentence)
% \item name method (molecular dynamics), method justification?
% \item potential source Jang, reason???? ( contain hydrogen? LAMMPS?
%   )
% \item sticking coefficients
% \item structure of the work
%   \begin{itemize}
%   \item first description of plasmas and films
%   \item than an introduction to molecular dynamics
%   \item follow by the details on the potential, which is the key
%     feature of the simulation
%   \item (short intro lammps?)
%   \item numeric implementation ( tricks and own code extensions)
%   \item results
%     \begin{itemize}
%     \item implementation validation
%     \item creation of surfaces
%     \item sticking coefficients CF2, F, CF, CF3, CF4
%     \item influence on the global model
%     \end{itemize}
%   \end{itemize}
% \item Conclusions - results, problems
% \item outlook - extension, hydrogen ...
% \end{itemize}




% \todoin{Hier ist noch viel zu tun! Klare Motivation, dann Ableitung
% der Fragestellung, wie wird diese bearbeitet, was gibt es schon und
% was kann man Neues erwarten. Daraus dann die Struktur der Arbeit
% ableiten, die hier auch aufzuführen ist. Insgesamt würde ich so 5
% Seiten erwarten, evtl.auch mehr}

% The main topic of this work are the computational investigation of
% \textit{surface reactions} in \textit{carbon fluorine plasmas}.
% This is done via the method of \textit{molecular dynamics}.

% The main motivation to the work are investigation done by Gunnar
% Bandelow.  The developed global model pred



% \begin{itemize}
% \item connection to 0d model
% \end{itemize}

% Limitations
% \begin{itemize}
% \item no oxygen or silicone so model is unsuited for the
%   investigation of etching processes
% \item integrated model but plasma not the only application
% \item creation of \ce{SiF4} and \ce{CF2} to \ce{C2F4}
% \end{itemize}

% Structure
% \begin{itemize}
% \item \ce{CF4} Plasmas widely used in industry
% \item two cases: etching and film deposition
% \item important particles are the radicals of \ce{CF4}, change of
%   radicals due to addition of hydrogen
% \item sticking coefficients are mainly estimated
% \item global model to show influence of sticking on the composition
%   (picture)
% \item concentration in this work on pure fluorocarbon films
%   (literature)
% \end{itemize}


% \todoin{hier viel präziser und genauer - problem das sticking etc
% noch nicht eingefuehrt?}  As the exact parameters of
% particle-surface interaction (plasma-wall-interaction) are not known
% in detail, the goal of this work is to estimate the reactions for
% some selected species.  These reactions are likely to introduce new
% sinks and sources for all species in the plasma, which influences
% are investigated in Bandelow's global model.  Which possible
% reactions could occur at the surface is presented in the next
% section together with a discussion about their observability in the
% current simulation.

% The purpose of this work is the development of the surface part in
% an integrated global model which simulates a carbon fluorine plasma
% as whole.
% % \todo{die beiden teilsätze haben nicht viel miteinander zu tun}
% However, this is not the only field of application as the result
% will be usable for other methods.

% The analyzed amorphous fluorine carbon films and polymers are of
% higher interest due to some convincing properties.  These are for an
% organic structure their relative high thermal stability of up to
% \SI{530}{\kelvin}\cite{dupontproperties}.  Secondly, they have an
% ultra low surface tension due to weak polarizability of the
% contained fluorine atoms.  These effects are commonly used in
% products like GORE-TEX\cite{gore1976process}.  Thirdly, fluorocarbon
% films have also a low permittivity, that is why they are called a
% \textit{low-k-material}, which are getting more and more interesting
% for the computer chip manufacturing industry as a way to reduce
% parasitic capacitances in shrinking structures.

% Often plasma parameters are only partly known, e.g. the density of
% radicals but not their energy distribution is accessible.  \todo{man
% ist oft schon froh, wenn man verlässlich die temperatur bestimmen
% kann, das klingt daher schon komisch. den nächsten satz versteh ich
% nicht, hab da mal was geändert} In order to better understand low
% temperature fluorocarbon plasmas it would be possible to calculate
% from measured growth rates the energy of the inclining radicals if
% the surface reaction rates depending on the impinging energy are
% known.

% The influence of sticking coefficients is quiet strong as a
% sensitivity study by Bandelow in his global model shows.  Additional
% sinks act through the introduced sticking coefficients so the system
% get a new equilibrium with densities different from a bulk only
% consideration.  Results are shown in Figure~\ref{fig:stick_start}
% where the unperturbed case in red is plotted with different
% strengths of sticking (other colors).  Hereby the sticking
% coefficients of \ce{CF3} ($\beta_{\cee{CF3}} = 0.1$), \ce{CF2}
% ($\beta_{\cee{CF2}} = 0.15$) and \ce{CF} ($\beta_{\cee{CF}} = 0.2$)
% are varied with a scaling factor.  The coefficients of \ce{F} and
% \ce{C} is thereby left constant with $\beta = 0.25$.  This means
% that the loss channels are amplified for the three varied species.
% All observed species in the global model show changes in their
% density but in Figure~\ref{fig:stick_start} are only three different
% plotted due to their different variation.

% The density of the feed-gas \ce{CF4} seems relative unaffected by
% the scaling factor to the sticking coefficients.  But as it is the
% greatest density in the system changes of about a percent will cause
% great changes to other species.  Interestingly with higher sticking
% the density also \textit{increases} slightly for \ce{CF4}.  The same
% trend is shown more significantly by the density of \ce{F2} which
% density is more than doubled in this survey.  The effect of higher
% sticking coefficients is contrary on the density of \ce{C2F4}, which
% exists a lot less.  Additional results about this sensitivity study
% are discussed and classified in detail in
% section~\ref{sec:infl-stick-coeff} in comparison with the sticking
% coefficients obtained during this work.

% \begin{figure}[htb]
%   \captionsetup{indent=-1.0cm}
%   \centering
%   \includegraphics{tikz/histos/sensitive-crop}
%   \caption{Influence of sticking one the density of selected species
%   in the global model.  The numbers in the legend are the scaling
%   factors for an assumed stick of $\beta_{\cee{CF3}} = 0.1$,
%   $\beta_{\cee{CF2}} = 0.15$ and $\beta_{\cee{CF}} = 0.2$. The
%   sticking coefficients $\beta_{\cee{F}} = 0.25$ and
%   $\beta_{\cee{C}} = 0.25$ are constant.}
%   \label{fig:stick_start_old}
% \end{figure}

% %%% Local Variables:
% %%% mode: latex
% %%% TeX-PDF-mode: t
% %%% TeX-master: "../main"
% %%% End:
