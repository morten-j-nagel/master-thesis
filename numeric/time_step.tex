
\section{Estimation of the time step}
\label{sec:estimation-time-step}

% Commonly no fixed time step is used in the integrator but it is
% rescaled during the simulation.  On one hand this is motivated by
% minimizing   On the other hand n
The choice of the time step for MD is critical due to the request for
a balance between numerical accuracy and needed run-time. A large
time step reduces the number of potential calculations needed to
simulate the desired time, because every potential call is
computationally very costly.  No arbitrary large time step can be
chosen because a larger time step will lead to larger integration
errors.  By defining a maximal acceptable error, the time step can be
derived from the current largest velocity in the system.

This maximum error defines also the accepted deviations from the
conservation equations of energy and momentum.  These deviations
should be so small that their impact on the system is still
negligible.  To guarantee that a particle is
interacting with the proper potential and to avoid that it will
penetrate through a potential wall due to numerical errors, it is
necessary to estimate the largest acceptable time step.  An
appropriate time step will guarantee a smoother change of the force
and prohibit sudden artificial accelerations of some particles which
could cause instabilities in the molecules.  One common technique is a
Taylor expansion around the equilibrium distance $r_{eq}$.  Since in
equilibrium the net force is zero the first derivative will vanish and
the resulting form can be approximated by a harmonic oscillator:
\begin{subequations}
  \begin{align}
    \label{eq:78}
    \tilde \Phi\mapar{r} &= \Phi_{min.} + \frac{1}{2}
    \left.\frac{\partial^2
        \Phi\mapar{r}}{\partial r^2}\right|_{r=r_{eq.}} + O\mapar{r^3}\\
    \label{eq:79}
    &\overset{!}{=} V_0 + \frac{m \cdot \omega^2}{2} \cdot \mapar{r -
      r_{eq.}}^2 \\
    \intertext{resulting in the period of oscillation $\tau =
      \frac{2 \cdot \pi}{\omega}$}
    \label{eq:80}
    \tau &= 2 \pi \cdot \sqrt{\frac{m}{\left.\frac{\partial^2
            \Phi\mapar{r}}{\partial r^2}\right|_{r=r_{eq.}}}}
  \end{align}
\end{subequations}
Assuming for the potential \eqref{eq:10} a bond order term of $b_{ij}
= 1$ one gets with \eqref{eq:18} and \eqref{eq:19} within the
cut-off region the second derivative for the intra-molecular
interaction:
\begin{align}
  \label{eq:81}
  \begin{split}
    \frac{\partial^2 \Phi_{ij}}{\partial r_{ij}^2} \, &= \, A_{ij}
    \,\mabra{\alpha_{ij}^2 + \frac{\alpha_{ij}^2 Q_{ij}}{r_{ij}} +
      \frac{2 \alpha_{ij} Q_{ij}}{r_{ij}^2} + \frac{2
        Q_{ij}}{r_{ij}^3}}\,\exp{\mapar{- \alpha_{ij} r_{ij}}}
    \\
    &\qquad- \sum_{n} \, B_{ij}^{(n)} \mapar{\beta_{ij}^{(n)}}^2 \,
    \exp \left( - \beta_{ij}^{(n)} r_{ij} \right)
  \end{split}
\end{align}
With the coefficients from Table \ref{tab:brenner} the periods
for the carbon-carbon and the hydrogen-hydrogen interaction are
\begin{align}
  \label{eq:82}
  \tau_{CC} &\approx \SI{40}{\femto\second} & \qquad \tau_{HH}
  &\approx \SI{11}{\femto\second}
\end{align}
Doing the same for the Lennard-Jones part of the potential one gets
\begin{subequations}
  \begin{align}
    \label{eq:76}
    \tilde\Phi\LJ\mapar{r} &= \epsilon_{ij} + \frac{18 \cdot
      2^{\frac{2}{3}}}{\sigma_{ij}^2}
    \cdot \epsilon_{ij} \mapar{r - r_{eq.}}^2 + O\mapar{r^3}\\
    \label{eq:77}
    \Longrightarrow \quad \tau_{CC} & \approx \SI{3.7}{\pico\second}.
  \end{align}
\end{subequations}
As a rule of thumb the shortest time resolved in the system should be at
least ten times larger than the time step.  Therefore, a time step of
the order of $\Delta t = \SI{1}{\femto\second}$ should be used.  

Unfortunately, this assumption is only valid around the point of
equilibrium.  Consequently, this time step is only sufficient for
cases without bond formation or higher temperatures.  In these cases
smaller time steps are needed to resolve the dynamics adequately.  One
exception is the binary collision simulation presented in
chapter~\ref{sec:potent-valid} where the atoms are accelerated by
the whole potential. Likewise an adequate estimate of the transformed
kinetic energy \eqref{eq:36} can be obtained from the potential depth
\begin{align}
  \label{eq:83}
  v &= \sqrt{\frac{2 \Phi_{\mathtt{min. ~CC}}}{m}} \approx
  \SI{0.071}{\angstrom\per\femto\second} \, .
\end{align}

Assuming to need at least one hundred time steps crossing the
interaction region with a width of about \SI{1}{\angstrom} (compare
with Fig.~\ref{fig:potential}) one gets as an upper limit
\begin{align}
  \label{eq:84}
  \tau_{CC} &\approx \SI{0.142}{\femto\second}.
\end{align}
Since bond formation and breaking are the processes to be resolved a
constant time step
\begin{align}
  \label{eq:85}
  \Delta t = \SI{0.1}{\femto\second}
\end{align}
is chosen.  Further tests in chapter~\ref{sec:potent-valid}
 will demonstrate the validity of this choice.

%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t
%%% TeX-master: "../main"
%%% End: 
