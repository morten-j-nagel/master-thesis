\section{Thermostat \& Barostat}
\label{sec:thermostat}

In this section tools will be briefly described to control the
temperature or pressure of the simulation based on
\cite{springerlink:10.1007/978-3-540-74686-7_1}.  For the generation
of surfaces (see section \ref{sec:generation-surfaces}) different
phases of temperature and pressure control are needed.  Due to the
conservation properties of the Velocity-Verlet integrator (see
\ref{sec:veloc-verl-algor}) the number of particles $N$, the volume
$V$ and Energy $E$ is preserved. Therefore, in terms of statistics
such a system represents a \textit{micro-canonical ensemble}
($NVE$ ensemble).  Sometimes it is desired to keep the temperature
constant although the system may gain or lose energy, like during the
formation of bonds.

To limit the total kinetic energy of the system a control of the
temperature is used, resulting in a \textit{canonical ensemble} ($NVT$
ensemble).  This control is implemented by rescaling of the particle
velocities.  The rescaling factor is calculated from thermodynamic
coupling to a \textit{Thermostat} and was derived by
Berendsen\cite{berendsen:3684} as:
\begin{align}
  \label{eq:70}
  \lambda &= \sqrt{1 + \frac{\Delta t}{\tau_T}
    \mapar{\frac{T_{ext}}{T_{cur.}} - 1}}.
\end{align}
Here, $T_{cur}$ is the current system temperature computed through
eq. \eqref{eq:36}, $T_{ext}$ is the desired system temperature, $\Delta
t$ is the integration time step and $\tau_T$ is a scaling factor defining
the timescale until $T_{ext}$ is obtained.

In the same way Berendsen\cite{berendsen:3684} derived a control
factor for the coupling to \textit{pressure bath}, a so called
\textit{Barostat}:
\begin{align}
  \label{eq:72}
  \mu &= \mabra{1 + \frac{\beta \cdot \Delta t}{\tau_P}\mapar{P_{cur}
      - P_{ext}}}^{\frac{1}{3}}.
\end{align}
$P_{cur}$ is the current system pressure computed by \eqref{eq:62} and
$P_{ext.}$ is the desired system pressure.  Additional $\Delta t$ is
the time step for the integrator, $\beta$ is the isothermal
compressibility of the system.  This is the inverse of the
\textit{bulk modulus}, which is set in LAMMPS as $ \beta =
1 / 10$.  $\tau_P$ defines the timescale of pressure
rescaling.  With the factor $\mu$ all system coordinates $\vec r_i$
and the system size $\vec l$ are rescaled
\begin{align}
  \label{eq:92}
  \vec r_i \mapar{t + \Delta t} &= \mu \cdot \vec r_i \mapar{t} &
  \quad \vec l \mapar{t + \Delta t} &= \mu \cdot \vec l \mapar{t}.
\end{align}

General Berendsen advises to use scaling factors which are at least one
hundred time greater than the time step.  From the chosen time step
\eqref{eq:85} this results in
\begin{align}
  \label{eq:93}
  \tau_x > \SI{0.01}{\pico\second}
\end{align}
Actually this was the lower limit chosen for $\tau_T$ with the upper
limit of $\tau_T = \SI{20}{\pico\second}$.  The barostat has a much
shorter response time.  Therefore, at least $\tau_p =
\SI{50}{\pico\second}$ is chosen.  These values depend on the current
simulation parameters, for more details see chapter \ref{sec:results}.

These control algorithms can still produce problems and non-physical
results.  For the Thermostat a common problem is the so-called
\textit{icecube effect}, where the whole system rotates around the
center of mass.  With the simple form of the kinetic energy
calculation \eqref{eq:37} a high temperature is obtained while there
is only rigid body dynamics.  Improved algorithms like Nos\'e-Hover
thermo- and barostats\cite{doi:10.1080/00268979600100761} are existing
and sometimes used.  The penalty is increased numerical effort.

To be able to study the surface interactions in our system major
extensions of the LAMMPS code were necessary.  These will be
characterized in the following section. 

% \todo{Sehr rumpliger Schlusssatz, vll aufteilen auf 2} These were
% also used but as they also modify the integrator they result in
% quiet length routines, why they shall here only by cited as they are
% just scaled the velocities and system coordinates in a more complex
% way.  \todo{Übergang zum nächsten Kapitel}

%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t
%%% TeX-master: "../main"
%%% End: 
