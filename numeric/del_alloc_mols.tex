
\section{Allocation and removal of molecules}
\label{sec:alloc-remov-molec}

In this section the implementation of molecule allocation and deletion
will be presented.  To analyze sticking coefficients for a specific
molecule, this molecule is ``shot'' towards the surface with random
orientation and from a random position.  ``Shooting'' means that the
molecule is launched with a center-of-mass velocity equal to the
desired kinetic energy.  The molecule moves towards the surface and
one of the four cases from chapter~\ref{sec:expect-surf-react} will
happen (sputtering, reflection, sticking or fragmentation).  As some
surface reactions may need some time to happen the simulation is
continued.  The limit of the total time for one event is chosen such
that reactions within the numerical limits of MD, namely in terms of
run time and number of time steps, are finished.  Therefore, a total
time of \SI{5}{\pico\second} is simulated for every molecule.

% \todo{ An der Formulierung nochmal arbeiten: ...is its ... -
% better?}
At the end of each process the final state is analyzed with respect to
species and process distribution.  Algorithmically, the final state of
the molecule is diagnosed and remaining bonds are identified.  A
specific problem is that counting all particles above a certain level
to find reflected, sputtered or backscattered molecules is incorrect.
The reason for this is that strong intermolecular forces from
\ref{sec:adapt-interm-react} keep products transiently (for a
timescale of about $>\SI{5}{\pico\second}$) at and in the first layers
of the surface (see section~\ref{sec:sticking-results} for details).
After this time, these are released as well.  Therefore, all bonds are
analyzed and categorized and a dedicated analyze of the covalent bonds
is done identifying those contribution of molecules only transiently
bond.
\clearpage 

Two tasks arise:
\begin{enumerate}
\item repeated allocation of the molecule
\item diagnostics of the molecular structure.
\end{enumerate}
Repetition can be achieved by multiple execution of the same input
script with different seeds initializing the random number sequence.
The main task was here the creation of scripts to create videos,
analysis and plots and to prevent that older runs were overwritten by
new runs.  In addition, a direct way to allocate molecules in LAMMPS
with the AIREBO-potential had to be implemented.

\begin{figure}[htb]
  \centering \centering\centerline{%
    \begin{subfigure}[cb]{0.33\textwidth}
      \centering
      \caption{allocation with translation}%
      \label{fig:alloc_trans}%
      \input{tikz/numeric/alloc_mols/alloc_mols_a}
    \end{subfigure}%
    \hskip .50cm
    \begin{subfigure}[cb]{0.33\textwidth}
      \centering
      \caption{allocation with rotation}%
      \label{fig:alloc_rot}%
      \input{tikz/numeric/alloc_mols/alloc_mols_b}
    \end{subfigure}
    \hskip .50cm
    \begin{subfigure}[cb]{0.33\textwidth}
      \centering
      \caption{across periodic boundaries}%
      \label{fig:del_bound}%
      \input{tikz/numeric/alloc_mols/alloc_mols_c}
    \end{subfigure}%
  }
  \caption{Some applications of the implemented molecular source.  This
    can allocate molecules in different domains or across boundaries
    with translational or rotational energy.  Hereby, the energies can be a
    constant or sampled from a distribution.  In
    \ref{fig:del_bound} also the problem to compute bonds across
    boundaries is illustrated.}
  \label{fig:alloc_del_mol}
\end{figure}

Arbitrary molecules can now be loaded from an
old LAMMPS-output file (like a simulation of a single molecule)
into a C++-class.  This class has different functions which allow to
rotate the molecule with three arbitrary Euler angles or to add a certain
velocity to the center-of-mass or a angular momentum to the whole
molecule.  These molecules are controlled by a molecular source which
is generated from an input-file specifying the frequency, the kinetic
and rotational energy and their variance.
One molecule can be arbitrarily often included with different
parameters through this input-file. Likewise, any energy distribution
can be realized.

% \todo{meinst du wirklich "process" oder "processors" / is compute ?
% ja}
To allocate a molecule its random position is determined in the master
process within the starting region above the surface.  Now all
processes on the different processors check the topological structure
of this point with respect to the chosen parallelization.  The check
is done via a hard sphere with a center at the center-of-mass and a
radius of the most distant atom excluding possible intersections in
all domains.  In case there is no intersection conflict detected the
different atoms of the molecules are then assigned to these different
domains according to the previous analysis.  This procedure prevents
allocation of an atom near to a surface atom within distances
resulting in too large potential energies.

The second task is accomplished via a recursive function.  Starting
with one atom all its bounded neighbors are assigned the identical
molecule number.  These neighbors atoms are taken as starting
positions, repeating the procedure until all atoms in a domain have a
number.  One has to prevent endless loops by checking the assignment
status.  Also ghost atoms end the recursion procedure.  Free molecules
are identified by excluding bounded ones.  For this, every domain
checks if a molecule fulfills a deletion criteria like 'atoms of a
specific group being bounded in it' or 'no member outside of a
specific area'.  In case, a molecule can not be deleted directly
because it exists across domain boundaries each process checks to
which neighbor domains it belongs.  Finally, all bonds are identified
from a simple threshold distance one can distinguish between intra-
and inter-molecular bonds.

The implementation was done in LAMMPS modifying the source code.  Due
to the new option of a molecular source a box can be filled with
different molecules in a pre-defined fraction allowing the creation of
a sample surface (see Section~\ref{sec:generation-surfaces}).  Also, a
continuous bombardment of a surface is much easier because not every
individual molecule has to be defined in the input-file.

In this chapter the basics of the simulation tools were presented,
necessary for this work.  In particular, special adaptions of the
source code were described.  Now, all tools are at hand to address the
central question of this work, namely the analysis of surface
interaction in carbon fluorine plasmas.

%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t
%%% TeX-master: "../main"
%%% End: 
