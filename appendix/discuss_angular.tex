The sticking coefficients $\beta(\alpha)$ for different impact angles
at a single impact energy can be approximated from the sticking
coefficients $\beta(E)$ for impacts normal to the surface.  This
assumption holds true as long as no sputtering of the surface occurs.

\begin{figure}[htb]
  \centering\centerline{%
    \hskip 1cm %
    \begin{subfigure}[cb]{0.6\textwidth}
      \centering
      \caption{$\beta(\alpha)$ with $\mathcal{E} = \SI{50}{\eV}$}%
      \label{fig:stick_angle}%
      \input{plots/dumb_angle_est/stick_angle}
    \end{subfigure}%
    \begin{subfigure}[cb]{0.6\textwidth}
      \centering
      \caption{$\beta(\mathcal{E})$ with $\alpha = \SI{0}{\degree}$}%
      \label{fig:stick_energy}%
      \input{plots/dumb_angle_est/stick_energy}
    \end{subfigure}%
  }
  \caption{Sticking coefficient $\beta$ in dependence of molecule
    impact energy $\mathcal{E}$ and impact angle $\alpha$ normal to
    the surface at $\mathcal{E} = \SI{50}{\eV}$ for \ce{CH3}.  Via the
    relation \eqref{eq:64} the fit for $\beta(\mathcal{E})$ is plotted
    as $\beta(\alpha)$ and the other way round.}
  \label{fig:energy_angle}
\end{figure}

Assuming that only the energy of the velocity perpendicular to the surface
is important for the sticking coefficient $\beta$, one gets with the
angle $\alpha$ between the surface normal and the center of mass
velocity of the incoming molecule
\begin{align}
  \label{eq:64}
  E_{eff.} \big( \alpha \big) &= E_\perp = \frac{m
    \cdot v_\perp^2}{2} = \frac{m \cdot (v\cdot\cos{\alpha})^2}{2} =
  E\cdot\cos^2{\alpha}.
\end{align}
This ansatz is inserted in the fitting formulas for \ce{CH3} in
Tichmann\cite{tichmann2009determination} to convert 
$\beta(\mathcal{E})$ into $\beta(\alpha)$ (see Fig.~\ref{fig:energy_angle}).  As the fitting formula is
only valid for energy larger than \SI{5}{\eV} an additional expression is
necessary to describe angles close to \SI{90}{\degree}.  Fortunately,
Sharma\cite{sharma2007hydrocarbon} has made similar calculations
with the same code\cite{nordlund1996formation} and for the same
hydrogen and carbon fraction $\kappa = 0.66$.  Since the sticking
coefficient at \SI{5}{\eV} differs between these papers by about \SI{40}{\percent}, only the
linear fit from the logarithmic scaled plot is taken from Sharma and the
fitting equation is expanded to
\begin{subequations}
  \begin{align}
    \label{eq:63}
    f(x) &= a \cdot \log{x} + b \\
    \label{eq:65}
    &= 0.0993 \cdot \log{x} + 0.358.
  \end{align}
\end{subequations}

Because the normal impact with \SI{5}{\eV} is equivalent to an impact
with \SI{50}{\eV} at an angle of \SI{71.6}{\degree}, the error in
Fig.~\ref{fig:stick_angle} for smaller angles may be introduced by sticking at surfaces bumps.
Nonetheless, the maximum difference in both figures is around $\approx
0.1$, which is smaller then the difference between
\cite{tichmann2009determination} and \cite{sharma2007hydrocarbon} at
\SI{5}{\eV} with $\approx 0.4$.  Therefore, for small energies without
sputtering relation \eqref{eq:64} should give an approximation within
the statistical errors of the simulation.  

\clearpage
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../main"
%%% End: 
