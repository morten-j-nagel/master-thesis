
\chapter{Appendix}
\label{sec:rest}
%\todoin{order appendix + explain some formulas}
\tocless
\section{Basic Equations}
\label{sec:formulas}

\tocless
\subsection{Thermodynamic properties}
\label{sec:therm-prop}

The total kinetic energy and therefore the temperature is calculated
from the kinetic energy of each particle.  In the calculation of
temperature the number of dimensions are neglected in the degrees of
freedom $d$, as the orientation of the coordinate system can be chosen
arbitrarily.
\begin{align}
  \label{eq:36}
  \mathcal{E}_{kin} &= \sum_i \frac{m_i \cdot v_i^2}{2} \\
  \label{eq:37}
  &= \frac{d}{2} \cdot (N - 1) \cdot k_B \cdot T
\end{align}

The pressure is calculated with the viral term $\sum_i^N \sum_{j(>i)} r_{ij} \cdot
F_{ij}$ \, \Bigg($F_{ij} = - \left. \frac{d \phi_{ij}}{d r} \right|_{ij}$ - force
between particle i and j\Bigg)
\begin{align}
  \label{eq:62}
  \mathcal{P} &= \frac{N \cdot k_b \cdot T}{V} + \frac{\sum_i^N \sum_{j(>i)} r_{ij} \cdot
F_{ij}}{d \cdot V}\, .
\end{align}

\tocless
\subsection{Unbiased estimators for statistical analysis}
\label{sec:bias-estim}

The following statistical estimators are used to calculated values
during the computation
with a varying number of runs $\omega_i$ per sample $i$:
\begin{itemize}
\item 
weighted arithmetic mean
\begin{align}
  \label{eq:68}
  \bar x &= \frac{\sumni{\omega_i x_i}}{\sumni{\omega_i}}
\end{align}
\item weighted variance equal to squared standard derivation
\begin{align}
  \label{eq:67}
  \sigma^2 &= \frac
  {  \sumni{\omega_i x_i^2}
      \cdot \sumni{\omega_i} - \mapar{ \sumni{\omega_i x_i} }^2 }
  { \mapar{ \sumni{\omega_i} }^2 }
\end{align}
\item weighted confidence interval of the arithmetic mean
\begin{align}
  \label{eq:66}
  \bar \sigma ^2 &= \frac
  { \mabra{ \sumni{\omega_i x_i^2}
      \cdot \sumni{\omega_i} - \mapar{ \sumni{\omega_i x_i} }^2 }
    \cdot \sumni{\omega_i^2} }
  {\mabra{ \mapar{ \sumni{\omega_i} }^2 - \sumni{\omega_i^2} } \cdot
    \mapar{ \sumni{\omega_i} }^2 }
\end{align}
\end{itemize}

\tocless
\subsection{Derivation of a new switching term}
\label{sec:deriv-new-switch}

The system of equations follows from the constraints for the values
and derivatives.   
With $t\left(r_{ij}\right) = \frac{r_{ij} - r^{
    \mathtt{min}}_{ij}}{r^{ \mathtt{max}}_{ij} -
  r^{\mathtt{min}}_{ij}}$ one gets for the calculation of the
switching function $f(x)$ the system of equations  with $t\mapar{r^{
    \mathtt{min}}_{ij}=0}$ and $t\mapar{r^{ \mathtt{max}}_{ij}=1}$:
\begin{align}
  \nonumber
  f(0) &= 1 & f(0.5) &= 0.5 &f(1) &= 0\\
  \label{eq:73}
  f'(0) &= 0 & & &f'(1) &= 0 \\
  \nonumber 
  f''(0) &= 0 & & &f''(1) &= 0
\end{align}
For the Hermite interpolation the solution is written with
Horner's method 
\begin{align}
  \label{eq:74}
  p(x) &= 1 + x^3 \Big( x ( 15 - 6x) - 10 \Big)
\end{align}

The original and the new switching functions are compared in
Figure~\ref{fig:eval_switch}.  The occurring bumps in the derivative lead to
a modification of the force acting in the cut-off region.  As the
force is strongly increased in comparison to the case without cut-off
function wrong stress tensors may be computed
operating in this regime.  For such cases, the switch function is
multiplied with the force\cite{Mrovec2007230}.
\begin{figure}[htb]
  \captionsetup{width=\textwidth}
  \centering\centerline{%
  \begin{subfigure}[cb]{0.6\textwidth}
    \centering 
     \caption{switch functions}%
    \label{fig:switches}%
    \input{plots/appendix/switches}
  \end{subfigure}%
  \begin{subfigure}[cb]{0.6\textwidth}
    \centering 
    \caption{derived switch functions}%
    \label{fig:derived_switches}%
    \input{plots/appendix/derived_switches}
  \end{subfigure}%
}
\caption{In \ref{fig:switches} the evaluated switches are presented.
  In \ref{fig:derived_switches} their derivations.}
\label{fig:eval_switch}
\end{figure}

\tocless
\subsection{Plasma reactions}
\label{sec:plasma-reactions-1}

\input{appendix/reactions}

\tocless
\subsection{Relation between impact energy and impact angle}
\label{sec:depend-betw-impact}

\input{appendix/discuss_angular}

\tocless
\section{Coefficients Tables}
\label{sec:tables}

\begin{table}[H]
  \centering
  \input{appendix/tables/rebo_tab}
  \caption{Values used for the covalent part of the
    AIREBO-potential.}
  \label{tab:brenner}
\end{table}

\begin{table}[H]
  \centering
  \input{appendix/tables/lj_tab}
  \caption{Values used for the Lennard-jones-potential in the
    AIREBO-potential.}
  \label{tab:lj1}
\end{table}

\begin{table}[H]
  \centering
  \input{appendix/tables/loss_F}
  \caption{Loss probability for additional surface molecules during
    the computation of the sticking coefficient for atomic fluorine.}
  \label{tab:loss_f}
\end{table}
%\clearpage

\tocless
\section{Additional Figures}
\label{sec:pictures}

\tocless
\subsection{Atomization energy validation}
\label{sec:atom-energy-test}

To test the potential implementation atomization energies for various
configurations are shown in
Figure~\ref{fig:atom_all} (see section~\ref{sec:potent-valid}).  For
hydrocarbons the agreement is excellent, but when fluorine is
bonded at carbon atoms with four bonds the deviation gets large.   
\begin{figure}[h]
  \centering
  \includegraphics[height=0.7\textheight]{tikz/appendix/atomization_energies/histo_energies-crop}
  \caption{Atomization energies for selected species.}
  \label{fig:atom_all}
\end{figure}

\tocless
\subsection{Frequency analysis of the barostat}
\label{sec:freq-analys-barost}

The Discrete Fourier Analysis of the volume oscillation driven by the
barostat is shown in Figure~\ref{fig:dft_baro}(see
section~\ref{sec:probl-with-barost}).  Low frequencies are stronger
populated.  In the auto-correlation function no periodic structure
appears (see Figure~\ref{fig:autocorr_baro}).  The process looks like
a ``moving average process'', but no further analysis was done. 

\begin{figure}[H]
  \captionsetup{width=7in}
  \centering\centerline{%
    \begin{subfigure}[cb]{0.6\textwidth}
      \centering
      \caption{}%
      \label{fig:dft_small}%
      \input{plots/failed_barostat/dft_big}
    \end{subfigure}%
    \begin{subfigure}[cb]{0.6\textwidth}
      \centering
      \caption{}%
      \label{fig:dft_big}%
      \input{plots/failed_barostat/dft_small}
    \end{subfigure}%
  }
  \caption{Discrete Fourier Analysis during
    the failed barostat relaxation.}
  \label{fig:dft_baro}
\end{figure}

\begin{figure}[hbt]
  \captionsetup{width=7in}
  \centering\centerline{%
    \begin{subfigure}[cb]{0.6\textwidth}
      \centering
      \caption{}%
      \label{fig:corr_small}%
      \input{plots/failed_barostat/corr_big}
    \end{subfigure}%
    \begin{subfigure}[cb]{0.6\textwidth}
      \centering
      \caption{}%
      \label{fig:corr_big}%
      \input{plots/failed_barostat/corr_small}
    \end{subfigure}%
  }
  \caption{ Auto-correlation function of the oscillating temperature
    during the failed barostat run Fig.~\ref{fig:fail_bar}. }
  \label{fig:autocorr_baro}
\end{figure}

\tocless
\subsection{Additional sample}
\label{sec:additional-sample}
In Figure~\ref{fig:mol_surf_inter} a generated surface with $\kappa =
1.2$ is shown.  This surface was produced through random allocation of
fluorine and carbons and is an intermediate state between surface 1
and 2 in Figure~\ref{fig:mol_surf}.  The large red molecule consists of
over one third of all carbon atoms and is the carrying skeleton of the
surface.  Surface molecules like the orange one may be detached
through fast impinging molecules. 

\begin{figure}[htb]
  \centering
  %\captionsetup{indention=-1.6cm}
  \input{plots/results/structure_surface_1.1/make_structure1}
  %\input{plots/results/structure_surface_2.1/make_structure3}
  \caption{Surface with $\kappa = 1.2$ generated by random
    allocation of atomic fluorine and carbon. }
  \label{fig:mol_surf_inter}
\end{figure}

%\input{appendix/ways_valid}

%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t
%%% TeX-master: "../main"
%%% End: 
