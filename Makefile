sections = theory appendix numeric results
plots = 
main = main
main_name = plasma-wall_interaction_in_carbon-fluor-plasmas

vpath %.tex $(sections)
vpath %.glt plots
vpath %.eps plots

vpath %.tex $(addprefix tikz/,$(section)) $(addprefix plots/,$(section))plots tikz
#vpath %.tex $(tikz_dirs)
#vpath %.tex plots

tikz_minimal = minimal_tikz

all_tex = $(wildcard */*.tex)

all_tikz = $(wildcard tikz/*/*.tex)
all_tikz += $(wildcard tikz/*.tex)
all_tikz += $(wildcard tikz/*/*/*.tex)

#tikz_dirs := $(wildcard tikz/$(sections)/*/)

plots += $(wildcard plots/*/*.glt)
plots += $(wildcard plots/*/*/*.glt)
plot_dirs := $(wildcard plots/*/)
all_plots :=  $(wildcard plots/*/*.tex)
all_plots +=  $(wildcard plots/*.tex)
all_plots +=  $(wildcard plots/*/*/*.tex)

images=$(plots:.glt=.eps)

.PHONY: $(main).pdf

show: $(main).pdf
	xdg-open $<

%.bbl: %.aux
	bibtex $*

$(main): $(main).pdf
	make all

all: tikz
	make main.pdf
	make main.bbl
	rm -f main.pdf
	make main.pdf
	rm -f main.pdf
	make main.pdf

$(main).pdf: $(images) $(all_tex) $(main).tex 
	pdflatex --shell-escape $(main)

sec-%.pdf : $(images) %.tex $(wildcard %/*.tex)
	pdflatex --shell-escape --jobname=sec-$* \
	"\includeonly{$*/$*}\input{$(main)}"
	make sec-$*.bbl
	rm -f sec-%.pdf
	pdflatex --shell-escape --jobname=sec-$* \
	"\includeonly{$*/$*}\input{$(main)}"
	rm -f sec-%.pdf
	pdflatex --shell-escape --jobname=sec-$* \
	"\includeonly{$*/$*}\input{$(main)}"

plots: $(images)

%.eps : %.glt
	@echo Generating $<
	echo "get_path(file) = sprintf(\"./${<D}/%s\",file); load '$<'" | gnuplot 
pdf_plots : plots
	make make_pdf_plots

# reevluate all tex pdf files
make_pdf_plots : $(all_plots:.tex=-preview.pdf)

plots/%-preview.pdf : %.tex
	@echo Generating $<
	pdflatex --shell-escape --jobname="$(<:.tex=-preview)" \
	"\documentclass[a4paper,12pt]{scrbook}\input{main/packages}\input{main/def}\input{main/pdfoptions}\externaldocument{$(main)}\begin{document}\input{$(<:.tex=)}\end{document}"	

clean-sec: $(addprefix clean-,$(sections))

clean-% : 
	rm -f sec-$*.*

$(sections) : % : sec-%.pdf
	xdg-open $<

echo_files:
#	@echo $(all_tex)
	@echo $(all_tikz:.tex=.pdf)
	@echo $(plots)
	@echo $(all_plots:.tex=.pdf)

tikz :  $(all_tikz:.tex=.pdf)

tikz/%.pdf : %.tex
	@echo $(*) $(<D)
	pdflatex --shell-escape --jobname="$(<:.tex=)" \
	"\documentclass[a4paper,12pt]{scrbook}\input{main/packages}\input{main/def}\input{main/pdfoptions}\externaldocument{$(main)}\begin{document}\input{$(<:.tex=)}\end{document}"	

